<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'wp_liberalcars');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c3n=Yes+DL~e$Qd*6{gIbz)$a:dXKx|Sp-?c{-Go^ul<f5F2j:ME|/2>`,i11f2A');
define('SECURE_AUTH_KEY',  'n>@.u%w+s.n};Si3!s =V@Pe_,!C*)OFQc]#flaUO3pyo=e!|QW^vy)M?8lmZ=Cr');
define('LOGGED_IN_KEY',    'Ob#m{~)o4A(E^]dI1<q .SGb!SMxb2wB^@I9kWFEgJ(O{uxg:7|j&F~f6kh-lh#Q');
define('NONCE_KEY',        'p026Cqa>TfK|nE~+~vkf;l|}E4&w0I=xdg|mI@Rs0 O.4/,NE6Oe:-P:<M(nUY)J');
define('AUTH_SALT',        'G#pJcb~~@%{#V&>}T~l*<GHTO)7%{&s*(1wE}+9kVDBj$ |(>S78U]E~!!-7vF#G');
define('SECURE_AUTH_SALT', '/Vx8<2f(5&w5-4h7q~_H}7N  Jd1vNdko9#]sr6vK1k7di4^i5fEg]E`Pa4,M^|=');
define('LOGGED_IN_SALT',   '/n6Q~.tE7d=|<UUhm0/ZlX`m;Fsn#V%aslogk:CxRY]2e?m-^8V9J+2;c9<$C@{3');
define('NONCE_SALT',       'P6w+<[2eG$AV|!g=_uTg4fJV,ZC1Ac~v5h8[ya@|()#|uYT|Y_|4sDiA]l5d.Np-');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', true);

/*自動アップデート停止*/
define( 'AUTOMATIC_UPDATER_DISABLED', true );

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
