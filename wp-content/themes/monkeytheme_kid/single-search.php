<?php get_header('hoan'); ?>
<?php setPostViews(get_the_ID()); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php $fieldObjs = get_field_objects();
	$fields = array();
	if($fieldObjs && !empty($fieldObjs)){
		foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
	}
	$headerImage = getObjectValue($fields, 'header_image');
	$fieldShops = array();
	if(isset($fields['shop']) && $fields['shop'] > 0){
		$fieldShopObjs = get_field_objects($fields['shop']);
		if($fieldShopObjs && !empty($fieldShopObjs)){
			foreach($fieldShopObjs as $fieldName => $field) $fieldShops[$fieldName] = $field['value'];
		}
	}
	$title = get_the_title(); ?>
	<div class="keyvisual"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
		<div class="n-wrapper">
			<?php custom_breadblog(); ?>
			<div class="text-inner">
				<span>Car Details</span>
				<h2 class="ttl-key"><?php echo $title; ?></h2>
			</div>
		</div>
	</div>
	<?php get_template_part('includes/box', 'line'); ?>
	<div class="inforcar">
		<div class="n-wrapper ">
			<div class="clearfix">
				<div class="left">
					<?php if(isset($fields['image_list']) && !empty($fields['image_list'])){ ?>
						<ul class="bxslider">
							<?php foreach($fields['image_list'] as $im){ ?>
								<li><img src="<?php echo $im['image']; ?>" alt="<?php echo $title; ?>" /></li>
							<?php } ?>
						</ul>
						<div id="bx-pager" class="clearfix ">
							<?php $i = -1;
							foreach($fields['image_list'] as $im){
								$i++; ?>
								<a data-slide-index="<?php echo $i; ?>" href="#"><img src="<?php echo $im['image']; ?>" alt="<?php echo $title; ?>" /></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="right">
					<div class="name"><?php echo $title; ?></div>
					<div class="sub"><?php echo getObjectValue($fields, 'car_no'); ?></div>
					<div class="box-price">
						<div class="text">本体<br />価格</div>
						<div class="number"><span><?php echo getObjectValue($fields, 'price'); ?></span>万円(税込)</div>
					</div>
					<?php if(!empty($fieldShops)){ ?>
						<div class="inquiry">お問い合わせ番号：<?php echo getObjectValue($fieldShops, 'free_dial'); ?></div>
						<a href="#" class="n-link trans">お問い合わせ</a>
						<div class="box-store">
							<div class="heading"><?php echo get_the_title($fields['shop']); ?></div>
							<div class="middle">
								<div class="tel">
									<span class="inner">TEL</span><span class="number"><?php echo getObjectValue($fieldShops, 'tel'); ?></span>
								</div>
								<p>お電話の際、ホームページを見た！とお伝え下さい。</p>
							</div>
							<div class="bottom">
								<p>営業時間 <?php echo getObjectValue($fieldShops, 'business_hours'); ?> <?php echo getObjectValue($fieldShops, 'regular_holiday'); ?>
									<br /><?php echo getObjectValue($fieldShops, 'street_address'); ?> <br><?php echo getObjectValue($fieldShops, 'closest_station'); ?></p>
								<a class="position trans" href="<?php echo get_permalink($fields['shop']); ?>"><span>お店の地図を見る</span></a>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="cover-tbl">
				<h2 class="ttl-tbl">車両基本情報</h2>
				
				<table class="tbl-info">
					<tr>
						<td>年式</td>
						<td>2002年12月</td>
						<td>2002年12月</td>
						<td>8人</td>
						<td rowspan="2" class="l-pink">修復歴</td>
						<td rowspan="2">無し<a target="_blank" href="#" class="n-link stl2 trans">修復歴車について </a></td>
					</tr>
					<tr>
						<td>型式</td>
						<td>TA-ACR30W</td>
						<td>車検</td>
						<td>2017年12月</td>
					</tr>
					<tr>
						<td>走行距離</td>
						<td><?php echo number_format(getObjectValue($fields, 'distance', 0)); ?>km</td>
						<td>色：外装</td>
						<td>シルバー</td>
						<td rowspan="6">装備・オプション</td>
						<td rowspan="6">
							<span class="tag">フル装備</span>
							<span class="tag">Wエアバッグ</span>
							<span class="tag">ABS</span>
							<span class="tag">後席モニター</span>
							<span class="tag">社外CD</span>
							<span class="tag">ワンセグ</span>
							<span class="tag">ETC</span>
							<span class="tag">キーレスエントリー</span>
							<span class="tag">社外アルミ</span>
							<a target="_blank" href="#" class="n-link stl2 mt10 trans">アイコンの説明 </a>
						</td>
					</tr>
					<tr>
						<td>燃料</td>
						<td>ガソリン</td>
						<td>色：内装</td>
						<td>グレー</td>
					</tr>
					<tr>
						<td>シフト</td>
						<td>IAT</td>
						<td>車台番号末尾3桁</td>
						<td>098</td>
					</tr>
					<tr>
						<td>駆動</td>
						<td>FF</td>
						<td>高さ</td>
						<td><?php echo getObjectValue($fields, 'height'); ?>cm</td>
					</tr>
					<?php $length = getObjectValue($fields, 'length');?>
					<tr>
						<td>排気量</td>
						<td>2,400cc</td>
						<td>長さ</td>
						<td><?php echo $length; ?>cm</td>
					</tr>
					<tr>
						<td><?php echo $length; ?>cm</td>
						<td>右</td>
						<td>幅</td>
						<td><?php echo getObjectValue($fields, 'width'); ?>cm</td>
					</tr>
					<tr>
						<td>保証プラン</td>
						<td colspan="5">品質への自信から、業界屈指の保証プランを用意。保証対象車両は全車走行距離無制限保証！ &nbsp;&nbsp;<a target="_blank" href="#" class="n-link stl3 stl2 trans">保証紹介ページへ </a>   </td>
					</tr>
				</table>
			</div>
		</div>
		
	</div>
	<!-- recommended -->
	<?php $news = new WP_Query(array(
        'posts_per_page' => 8,
        'post_type' => array('search'),
		'post__not_in' => array(get_the_ID())
    ));
    if($news->have_posts()) : ?>
	<div class="recommended second">
		<div class="col-w">
			<div class="block-title">
				<h3>最近チェックした車</h3> 
			</div>
			<div class="list-item clearfix">
				<?php while($news->have_posts()) : $news->the_post(); ?>
					<?php get_template_part('includes/loop', 'product'); ?>
				<?php endwhile;wp_reset_postdata(); ?>
			</div>
			<div class="link-more">
				<a href="<?php echo get_post_type_archive_link('search'); ?>">車両情報一覧を見る <i class="icon-arrow"></i></a>
			</div>
		</div>
	</div>
	<?php endif; ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>