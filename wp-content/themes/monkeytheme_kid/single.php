<?php get_header(); ?>
<?php setPostViews( get_the_ID() ); ?>
<script type="text/javascript">
$(function(){
　　if ($('div.blogbox').children().hasClass('p')) {
            $(this).html( $(this).html().replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, '<a href="$1">$1</a> ') );
}
});
</script>

<div id="dendo"> </div>
<!-- /#dendo -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
  <!--ループ開始-->
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <div class="kizi">
     <div class="entry-meta clearfix">
     <time class="entry-date date updated" datetime="<?php the_time('c') ;?>">
          <?php the_time('Y/m/d') ;?>
        </time>
        <div class="entry-category">
          <?php the_category(' ') ?>
        </div>
    </div>
    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>
    <?php get_template_part('sns');?>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <?php wp_link_pages(); ?>
  </div>
  <div style="padding:20px 0px;">
    <?php get_template_part('ad');?>
  </div>
  <div id="snsbox03">
    <!--SNSアカウント-->
    <h3 class="kanren">シェアする</h3>
    <div class="sns03">
      <ul class="snsb clearfix">
          <li style="margin-bottom:10px;">
            <?php
                $page_url=get_permalink();
                $page_title=trim(wp_title( '', false)); ?>
             <a href="http://twitter.com/share?text=<?php the_title(); ?>&amp;lang=ja&amp;url=<?php the_permalink() ?>" onclick="window.open(encodeURI(decodeURI(this.href)), 'tweetwindow', 'width=550, height=450, personalbar=0, toolbar=0, scrollbars=1, resizable=1' ); return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_tw.png" alt="twitter" width="130"></a></li>

          <li style="margin-bottom:10px;"><a onclick="window.open(this.href, 'FBwindow', 'width=550, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;" href="http://www.facebook.com/share.php?u=<?php echo $page_url ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_fb.png" alt="facebook"  width="130" ></a></li>

          <li style="margin-bottom:10px;"><a href="https://plus.google.com/share?url=<?php echo $page_url ?>" onclick="window.open(this.href, 'Gwindow', 'width=550, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_google.png" alt="google+" width="130"></a></li>

          <li style="margin-bottom:10px;"><a href="http://b.hatena.ne.jp/entry/<?php the_permalink(); ?>" class="hatena-bookmark-button" data-hatena-bookmark-layout="simple" data-hatena-bookmark-lang="en" title="<?php the_permalink(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_hatebu.png" alt="このエントリーをはてなブックマークに追加" width="130"/></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script></li>
      </ul>
    </div>
  </div>


<?php endwhile; else: ?>
<p>記事がありません</p>
  <?php endif; ?>

<!--Recent Post-->
<div class="single-posts recent-post"> 
  <!--関連記事-->
  <h3 class="kanren">最近書いた記事</h3>
  <div class="post-wrap clearfix">
        <?php
        $args = array(
            'posts_per_page' => 3,
        );
        $st_query = new WP_Query($args);
        ?>
        <?php if( $st_query->have_posts() ): ?>
            <?php while ($st_query->have_posts()) : $st_query->the_post(); ?>
        <article class="entry">
          <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>
            <?php echo get_the_post_thumbnail($post->ID, 'thumb101'); ?>
            <?php else: // サムネイルを持っていないときの処理 ?>
            <img src="<?php echo catch_that_image($post->post_content); ?>" width="101" />
            <?php endif; ?>
            </a>
          </div>
          <h4> 
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h4>
        </article>
        

        <?php endwhile;?>
        <?php else:?>
          <p>記事はありませんでした</p>
        <?php
          endif;
          wp_reset_postdata();
        ?>
  </div>
  <div class="view-all">
    <a href="<?php echo home_url(); ?>/blog/recent-posts" title="view all">もっと見る</a>
  </div>
</div>
<!--Recent Post--> 

<!--Related Post-->
<div class="single-posts related-post"> 
  <!--関連記事-->
  <h3 class="kanren">関連の記事</h3>
  <div class="post-wrap clearfix">
        <?php
          $categories = get_the_category($post->ID);
          $category_ID = array();
          foreach($categories as $category):
          array_push( $category_ID, $category->cat_ID);
          endforeach ;
          $args = array(
          'post__not_in' => array($post->ID),
          'posts_per_page'=> 3,
          'category__in' => $category_ID,
          'orderby' => 'rand',
          );
        $st_query = new WP_Query($args); ?>
        <?php
          if( $st_query->have_posts() ): ?>
        <?php
        while ($st_query->have_posts()) : $st_query->the_post(); ?>
        <article class="entry">
          <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>
            <?php echo get_the_post_thumbnail($post->ID, 'thumb101'); ?>
            <?php else: // サムネイルを持っていないときの処理 ?>
            <img src="<?php echo catch_that_image($post->post_content); ?>" width="101" />
            <?php endif; ?>
            </a>
          </div>
          <h4> 
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h4>
        </article>
        

        <?php endwhile;?>
        <?php else:?>
          <p>記事はありませんでした</p>
        <?php
          endif;
          wp_reset_postdata();
        ?>
  </div>
  <div class="view-all">
    <a href="<?php echo esc_url( add_query_arg( $category_ID, home_url() . '/blog/related-posts' ) ); ?>" title="view all">もっと見る</a>
  </div>
</div>
<!--Related Post--> 

</div>
<!-- END div.post -->
<?php get_footer(); ?>