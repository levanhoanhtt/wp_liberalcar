</div>
<!-- /#main -->
<!--バナーブログTOP記事一覧下-->
</div>
<div class="clear"></div>
<!-- /.cler -->
</div>
<!-- /#wrap-in -->

</div>
<!-- /#wrap -->
  <footer id="footer">
    <div id="footer-in">
      <div class="footer-widget clearfix">
        <div class="widget-1 sp-none">
          <?php dynamic_sidebar('footer-widget-1')?>
        </div>
        <div class="widget-2 sp-none">
          <?php dynamic_sidebar('footer-widget-2')?>
        </div>
        <div class="widget-3">
          <?php dynamic_sidebar('footer-widget-3')?>
        <div class="sp-none">
          <?php dynamic_sidebar('footer-widget-4')?>
        </div>
        </div>
      </div>
     
      <div class="footer-copyright">
        <p class="copy">Copyright(C) 2005-2016 ©INFACT CO.,LTD.All Rights Reserved.</p>
      </div>
    </div>
    <!-- /#footer-in -->
  </footer>
</div>
<!-- /#container -->
<?php wp_footer(); ?>

<?php if ( wp_is_mobile() ) : ?>
  <div class="footer-bottom">
    <ul>
      <li><a href="<?php echo get_site_url(); ?>/proceeds">サービス</a></li>
      <li><a href="tel:0356857311">電話する</a></li>
      <li><a href="/contact">お問合せ</a></li>
      <li><a href="<?php echo get_site_url(); ?>/about">会社案内</a></li>
    </ul>
  </div>
<?php endif; ?>

<div class="backtotop center">
  <img class="sp-none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/backtotop_pc.png" alt="backtotop">
  <img style="width: 60px" class="pc_none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/backtotop_sp.png" alt="backtotop">
</div>
<?php if ( wp_is_mobile() ) : ?>
<!-- スライドメニュー -->
<div id="sp_slide">
  <div class="slidemenu slidemenu-left">
      <div class="slidemenu-body">
        <ul class="slidemenu-content">
          <li class="blog-top"> <a href="<?php echo home_url();?>" title="トップページ">TOP</a> </li>
              <?php wp_nav_menu(array('theme_location' => 'navbar'));?>
        </ul>
      </div>
  </div>
  <div class="slidemenu slidemenu-right">
    <div class="slidemenu-body">
      <div class="slidemenu-content">
        <?php get_template_part('sidebar-sp'); ?>
      </div>
    </div>
  </div>

  <div class="pos">
    <span class="button menu-button-left">MAIN<br>MENU</span>
    <span class="button menu-button-right">SIDE<br>MENU</span>
  </div>
</div>
<script>
    var menu_left = SpSlidemenu({
      main : '#container',
      button: '.menu-button-left',
      slidemenu : '.slidemenu-left',
      direction: 'left'
    });

    var menu_right = SpSlidemenu({
      main : '#container',
      button: '.menu-button-right',
      slidemenu : '.slidemenu-right',
      direction: 'right'
    });

</script>

<?php endif; ?>

<!-- ここにsns02 -->
<?php if ( function_exists('wp_is_mobile') && wp_is_mobile() ) :?>
<?php else: ?>
<?php get_template_part('sns02');?>
<?php endif; ?>
<!-- ここまでsns02 -->


</body>
</html>