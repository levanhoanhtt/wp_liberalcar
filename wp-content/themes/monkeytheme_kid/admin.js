
google.load("feeds", "1");
var getRssFeed = function (_id, _urls, _length) {
	if(!_id || !_urls || (!(_urls instanceof Array)))	return;
	//変数
	var entriesArray = new Array();
	var complete = 0;
	//読み込み
	var init = function () { for (var e = 0; e < _urls.length; e++) getFeed(_urls[e], e)};
	
	function getFeed(_url, _num) {
		var feed = new google.feeds.Feed(_url);
		if(_length)	feed.setNumEntries(_length);
		feed.load(function(result) {
			if (!result.error) {
				html = '<p>ブログを書く前に読んでおきたい記事</p>';
				html += '<div id="admin_info" class="clearfix">';
				for (var i = 0; i < result.feed.entries.length; i++) {
					var entry = result.feed.entries[i];
					var pdate = new Date(entry.publishedDate);
					var Y = pdate.getFullYear();
					var m = pdate.getMonth() + 1;
					m = (m < 10)? '0' + m:m;//月数字を2桁に
					var d = pdate.getDate();
					d = (d < 10)? '0' + d:d;//日数字を2桁に
					var date = Y + '年' + m + '月' + d + '日';
					var eimg = ""; //画像取得（初期値設定）
					var imgCheck = entry.content.match(/(src="http:){1}[\S_-]+((\.png)|(\.jpg)|(\.jpeg)|(\.JPG)|(\.gif))/); //該当する拡張子のデータを画像として取得している
					if(imgCheck){
					eimg += '<img ' + imgCheck[0] + '" width="100%">'; //配列の1番目に格納されたデータを取得（つまり、1枚目の画像を取得）
			    } 
					html += '<ul>';
					html += '<li><a href="' + entry.link + '" target="_blank">' + entry.title + '</a></li>';
					html += '</ul>';

				}
				
				html += '</div>';
				entriesArray[_num] = html;
				complete++;
				if(complete == _urls.length){
					echo();
				}
			}
		});
	};
	//出力
	var echo = function () {
		var container = document.getElementById(_id);
		for (var e = 0; e < _urls.length; e++) 	container.innerHTML += entriesArray[e];
	};
	
	google.setOnLoadCallback(init);
};

var feed1 = new getRssFeed("feed1", ['http://www.infact1.co.jp/staff_blog/category/admin/feed/'], 6);