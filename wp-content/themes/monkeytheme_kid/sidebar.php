<div id="side">
  <div class="sidead">
    <?php if(is_mobile()) { ?>
    <?php } else { ?>
    <?php get_template_part('ad');?>
    <?php } ?>
  </div>

  <!-- menu-sidebar -->
  <div class="menu-sidebar-wrap">
    <!-- <h4 class="menu_underh2">Sitemap</h4> -->
    <?php 
    wp_nav_menu(array(
      'menu' => 'menu-sidebar',   // This will be different for you. 
      'container_id' => 'menu-sidebar', 
      'walker' => new CSS_Menu_Maker_Walker()
    )); 
    ?>
  </div>
  <!-- /menu-sidebar -->

  <div class="sidebar_banner">
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <?php endif; ?>
  </div>

  <!-- SNS -->
  <!--<div class="snsb-wrap">
    <h4 class="menu_underh2">公式SNSアカウント</h4>
      <ul class="snsb clearfix">
      <li style="padding-left:0;"><a href="https://twitter.com/INFACT_JP" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_tw.png" alt="twitter" width="58"></a></li>
      <li style="padding-left:0;"><a href="https://www.facebook.com/infact1" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_fb.png" alt="facebook" width="58"></a></li>
      <li style="padding-left:0;"><a href="https://plus.google.com/+Infact1CoJp/posts" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_google.png" alt="google" width="58"></a></li>
      <li style="padding-left:0;"><a href="https://www.youtube.com/channel/UCsTYY--2wDKKvYXDPt8wDig" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_you.png" alt="youtube" width="58"></a></li>
    </ul>
  </div>-->
  <!-- /SNS -->

  <div id="ad1">
    <div style="text-align:center;">
      <?php get_template_part('scroll-ad');?>
    </div>
  </div>

</div>
<!-- /#side -->