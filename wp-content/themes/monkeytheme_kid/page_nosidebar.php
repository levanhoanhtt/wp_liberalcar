<?php /* Template Name: Page nosidebar */ ?>
<?php get_header('nosidebar'); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>


  <?php endwhile; else: ?>
  	  <p>記事がありません</p>
  <?php endif; ?>
  <!--ループ終了--> 

<?php get_footer('nosidebar'); ?>

