<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php $fieldObjs = get_field_objects();
	$fields = array();
	if($fieldObjs && !empty($fieldObjs)){
		foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
	}
	$headerImage = getObjectValue($fields, 'header_image');
	$title = get_the_title(); ?>
	<div class="keyvisual banner-key04"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
		<div class="n-wrapper">
			<?php custom_breadblog(); ?>
			<div class="text-inner">
				<span>shop</span>
				<h2 class="ttl-key"><?php echo $title; ?></h2>
			</div>
		</div>
	</div>
	<?php get_template_part('includes/box', 'line'); ?>
	<div class="shop-morioka">
		<div class="col-w">
			<h2 class="ttl-value-cmn"><?php echo $title; ?></h2>
			<div class="w-full clearfix">
				<div class="w-left">
					<?php if(isset($fields['images']) && !empty($fields['images'])){ ?>
						<ul class="bxslider">
							<?php foreach($fields['images'] as $im){ ?>
								<li><img src="<?php echo $im['image']; ?>" alt="<?php echo $title; ?>" /></li>
							<?php } ?>
						</ul>
						<div id="bx-pager" class="clearfix ">
							<?php $i = -1;
							foreach($fields['images'] as $im){
								$i++; ?>
								<a data-slide-index="<?php echo $i; ?>" href="#"><img src="<?php echo $im['image']; ?>" alt="<?php echo $title; ?>" /></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="w-right">
					<div class="info-content">
						<?php if(isset($fields['service_list']) && !empty($fields['service_list'])){ ?>
							<div class="block-link-shop">
								<?php foreach($fields['service_list'] as $s){ ?>
									<a href="javascript:void(0)"><?php echo $s['title']; ?></a>
								<?php } ?>
							</div>
						<?php } ?>
						<div class="table-info-shop">
							<table>
								<tr>
									<td>店舗名</td>
									<td><?php echo $title; ?></td>
								</tr>
								<tr>
									<td>住所</td>
									<td><?php echo getObjectValue($fields, 'street_address'); ?></td>
								</tr>
								<tr>
									<td>最寄駅</td>
									<td><?php echo getObjectValue($fields, 'closest_station'); ?></td>
								</tr>
								<tr>
									<td>フリーダイヤル</td>
									<td><?php echo getObjectValue($fields, 'free_dial'); ?></td>
								</tr>
								<tr>
									<td>TEL</td>
									<td><?php echo getObjectValue($fields, 'tel'); ?></td>
								</tr>
								<tr>
									<td>FAX</td>
									<td><?php echo getObjectValue($fields, 'fax'); ?></td>
								</tr>
								<tr>
									<td>MAIL</td>
									<?php $email = getObjectValue($fields, 'email'); ?>
									<td><a target="_blank" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
								</tr>
								<tr>
									<td>営業時間</td>
									<td><?php echo getObjectValue($fields, 'business_hours'); ?></td>
								</tr>
								<tr>
									<td>定休日</td>
									<td><?php echo getObjectValue($fields, 'regular_holiday'); ?></td>
								</tr>
							</table>
						</div>
						<style>.info-text .ts-fab-wrapper{display: none;}</style>
						<div class="info-text">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if(isset($fields['google_map']) && !empty($fields['google_map'])){ ?>
		<div class="box-map">
			<div class="col-w">
				<div class="block-map">
					<?php echo $fields['google_map']; ?>
				</div>
				<div class="link-map">
					<a href="<?php echo getObjectValue($fields, 'google_map_url', 'javascript:void(0)'); ?>" target="_blank"><i class="icon-map"></i> 大きな地図で見る</a>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php if(isset($fields['access_list']) && !empty($fields['access_list'])){ ?>
		<div class="access-store">
		<div class="col-w">
			<div class="title">
				<i class="icon-map"></i>
				<h3>店舗までのアクセス</h3>
			</div>
			<div class="list-item clearfix">
				<?php $i = 0;
				foreach($fields['access_list'] as $a){
					$i++; ?>
					<div class="item">
						<div class="content-block">
							<div class="image">
								<a href="javascript:void(0)"><img src="<?php echo $a['image']; ?>" alt="<?php echo $a['title']; ?>" /></a>
							</div>
							<div class="info-text">
								<a href="javascript:void(0)"><span><?php echo $i; ?>.</span><?php echo $a['title']; ?></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if(isset($fields['staff_category']) && $fields['staff_category'] > 0){
		global $post;
		$news = get_posts(array('posts_per_page'=> 3,'category'=> $fields['staff_category']));
		if(!empty($news)){ ?>
			<div class="box-news">
				<div class="col-w">
					<div class="block-title">
						<h3>スタッフ紹介</h3>
						<!--<span>WHAT'S NEW</span>-->
					</div>
					<table class="list-news">
						<?php foreach($news as $post) : setup_postdata($post); ?>
							<tr>
								<td>
									<div class="date"><?php the_time('Y.m.d'); ?></div>
								</td>
								<td>
									<div class="text-new">ニュース</div>
								</td>
								<td>
									<div class="info-new">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
								</td>
							</tr>
						<?php endforeach; wp_reset_postdata(); ?>
					</table>
					<div class="link-more">
						<a class="active" href="<?php echo get_category_link($fields['staff_category']); ?>">詳しく見る <i class="icon-arrow"></i></a>
					</div>
				</div>
			</div>
		<?php }
	} ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>