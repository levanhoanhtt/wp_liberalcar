<?php /* Template Name: related-posts */ ?>
<?php wp_head(); ?>
<?php
if ( is_front_page() ) :
    get_header( 'home' );
elseif ( is_page( 'About' ) ) :
    get_header( 'about' );
else:
    get_header();
endif;
?>
<?php
$cats = array_map(function ($cat) {
  return (int)$cat;
}, $_GET);
?>

<div class="archive-posts cat-posts"> 

  <!--ループ開始-->
  <div class="cat-description"><?php echo category_description(); ?></div>
  <h2 class="cat-title">「関連の記事」 一覧</h2>
  
  <div class="post-wrap clearfix">
  	<?php
  	$args = array ( 'post_type' => 'post', 'post_status' => 'publish', 'category__in' => $cats,'order' => 'DESC', 'orderby' => 'post_date' );
  	query_posts( $args );
  	?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article class="entry">
      <div class="entry-wrap clearfix">

        <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

          <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>

          <?php

          $title= get_the_title();

          the_post_thumbnail(array( 285,190 ),

          array( 'alt' =>$title, 'title' => $title)); ?>

          <?php else: // サムネイルを持っていないときの処理 ?>

          <img src="<?php echo catch_that_image($post->post_content); ?>" width="285" height="190" />

          <?php endif; ?>

          </a> </div>

        <!-- /.sumbox -->

        <div class="entry-content">

          <h3 class="entry-title"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

            <?php the_title(); ?>

            </a></h3>

        </div>

        <!-- .entry-content -->

      </div>

    </article>

    <!--/entry-->


    <?php endwhile; else: ?>

    <p>記事がありません</p>

    <?php endif; wp_reset_query(); ?>
  </div><!--post-wrap-->

  <div style="padding:20px 0px;">

    <?php get_template_part('ad');?>

  </div>

  

  <!--ページナビ-->

  <?php if (function_exists("pagination")) {

pagination($wp_query->max_num_pages);

} ?>

  <!--ループ終了--> 

</div>

<!-- END div.post -->


<?php get_footer(); ?>
<?php wp_footer(); ?>
