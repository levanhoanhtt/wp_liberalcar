<?php get_header('hoan'); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/blog.css" type="text/css" media="all"/>
    <?php $headerImage = '';  ?>
    <div class="keyvisual archi"<?php if (!empty($headerImage)) echo ' style="background: url(' . $headerImage . ') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span>blog</span>
                <h2 class="ttl-key"><?php single_cat_title() ?></h2>
            </div>
        </div>
    </div>
<?php get_template_part('includes/box', 'line'); ?>
    <div class="new-value">
        <div class="n-wrapper">
            <div class="archive-posts cat-posts">
                <div class="post-wrap clearfix">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article class="entry">
                            <div class="entry-wrap clearfix">
                                <div class="sumbox">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                                        <?php if (has_post_thumbnail()): // サムネイルを持っているときの処理 ?>
                                            <?php $title = get_the_title();
                                            the_post_thumbnail(array(285, 190), array('alt' => $title, 'title' => $title)); ?>
                                        <?php else: // サムネイルを持っていないときの処理 ?>
                                            <img src="<?php echo catch_that_image($post->post_content); ?>" width="285"
                                                 height="190"/>
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="entry-content">
                                    <h3 class="entry-title">
                                        <a href="<?php the_permalink() ?>"
                                           title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                </div>
                            </div>
                        </article>
                    <?php endwhile;
                    else: ?>
                        <p>記事がありません</p>
                    <?php endif;
                    wp_reset_query(); ?>
                </div>
                <?php if (function_exists("pagination")) pagination($wp_query->max_num_pages); ?>
            </div>
        </div>
    </div>
<?php get_footer('hoan'); ?>