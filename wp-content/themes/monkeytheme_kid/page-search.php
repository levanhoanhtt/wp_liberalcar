<?php get_header('hoan'); ?>
<div class="keyvisual">
    <div class="n-wrapper">
        <?php custom_breadblog(); ?>
        <div class="text-inner">
            <span>Car Search</span>
            <h2 class="ttl-key">車在庫検索</h2>
        </div>
    </div>
</div>
<?php get_template_part('includes/box', 'line'); ?>
<?php $searchArgs = array();
$fields = acf_get_fields('group_58c0b3c848a24');
if($fields){
    $searchFeilds = array();
    $choiceFields = array();
    foreach($fields as $field){
        if(in_array($field['name'], array('maker', 'bodytype', 'shift', 'inspection', 'repair'))){
            $searchFeilds[$field['name']] = array(
                'label' => $field['label'],
                'values' => isset($field['choices']) ? (array)$field['choices'] : array()
            );
        }
        elseif(in_array($field['name'], array('opnavi', 'opsun', 'opseat'))){
            $value = '';
            if(isset($field['choices'])){
                foreach($field['choices'] as $i => $v){
                    $value = $i;
                    break;
                }
            }
            $choiceFields[$field['name']] = array(
                'label' => trim(str_replace(array('オプション', '：', 'オプション：'), '', $field['label'])),
                'value' => $value
            );
        }
    }
    if(!empty($searchFeilds)){ ?>
        <div class="box-search">
            <div class="form-inner">
                <form method="get" action="<?php echo get_post_type_archive_link('search'); ?>" id="searchForm">
                    <div class="checked-inner">
                        <div class="col-w">
                            <div class="select-inner">
                                <?php foreach($searchFeilds as $name => $arr){
                                    $value = isset($_GET[$name]) ? trim($_GET[$name]) : '';
                                    if(!empty($value)) $searchArgs[$name] = $value; ?>
                                    <div class="item-inner">
                                        <h3><?php echo $arr['label']; ?></h3>
                                        <select name="<?php echo $name; ?>">
                                            <option value="">お選びください</option>
                                            <?php foreach($arr['values'] as $i => $v){ ?>
                                                <option value="<?php echo $i; ?>"<?php if($i == $value) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="select-inner">
                                <?php $prices = array(20, 40, 60, 80, 120, 150, 200, 250, 300, 400);
                                $priceMin = (isset($_GET['price_min']) && $_GET['price_min'] > 0) ? $_GET['price_min'] : 0;
                                $priceMax = (isset($_GET['price_max']) && $_GET['price_max'] > 0) ? $_GET['price_max'] : 0;
                                $searchArgs['price_min'] = $priceMin;
                                $searchArgs['price_max'] = $priceMax; ?>
                                <div class="item-inner">
                                    <h3>価格</h3>
                                    <select name="price_min">
                                        <option value="0">0万円</option>
                                        <?php foreach($prices as $i){ ?>
                                            <option value="<?php echo $i; ?>"<?php if($i == $priceMin) echo ' selected="selected"'; ?>><?php echo $i; ?>万円</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="item-inner">
                                    <h3>~</h3>
                                    <select name="price_max">
                                        <option value="0">上限なし</option>
                                        <?php foreach($prices as $i){ ?>
                                            <option value="<?php echo $i; ?>"<?php if($i == $priceMax) echo ' selected="selected"'; ?>><?php echo $i; ?>万円</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php $distances = array(10000, 20000, 40000, 60000, 80000, 100000);
                                $distanceMin = (isset($_GET['distance_min']) && $_GET['distance_min'] > 0) ? $_GET['distance_min'] : 0;
                                $distanceMax = (isset($_GET['distance_max']) && $_GET['distance_max'] > 0) ? $_GET['distance_max'] : 0;
                                $searchArgs['distance_min'] = $distanceMin;
                                $searchArgs['distance_max'] = $distanceMax; ?>
                                <div class="item-inner">
                                    <h3>走行距離</h3>
                                    <select name="distance_min">
                                        <option value="0">0km</option>
                                        <?php foreach($distances as $i){ ?>
                                            <option value="<?php echo $i; ?>"<?php if($i == $distanceMin) echo ' selected="selected"'; ?>><?php echo number_format($i); ?>km</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="item-inner">
                                    <h3>~</h3>
                                    <select name="distance_max">
                                        <option value="0">上限なし</option>
                                        <?php foreach($distances as $i){ ?>
                                            <option value="<?php echo $i; ?>"<?php if($i == $distanceMax) echo ' selected="selected"'; ?>><?php echo number_format($i); ?>km</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php if(!empty($choiceFields)){ ?>
                                <div class="checkbox-inner">
                                    <div class="item-inner">
                                        <h3>オプション :</h3>
                                        <?php foreach($choiceFields as $name => $arr){
                                            $value = isset($_GET[$name]) ? trim($_GET[$name]) : '';
                                            if(!empty($value)) $searchArgs[$name] = $value; ?>
                                            <div class="item-inner">
                                                <label>
                                                    <input type="checkbox" name="<?php echo $name; ?>" value="<?php echo $arr['value']; ?>"<?php if($arr['value'] == $value) echo ' checked="checked"'; ?> />
                                                    <span></span>
                                                    <?php echo $arr['label']; ?>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="btn-inner">
                        <div class="col-w">
                            <div class="block-btn">
                                <input type="reset" class="btn-seach-1" value="条件をクリア" />
                            </div>
                            <div class="block-btn">
                                <input class="input-search" type="submit" value="この条件で検索" />
                                <button class="btn-seach-2" type="submit">
                                    <i class="icon_search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php }
}
$paged = (isset($_GET['pg']) && $_GET['pg'] > 0) ? $_GET['pg'] : 1;
$args = array(
    'posts_per_page' => 8,
    'post_type' => array('search'),
    'paged' => $paged
);
if(!empty($searchArgs)){
    $meta_query = array();
    foreach($searchArgs as $key => $value){
        if(!in_array($key, array('price_min', 'price_max', 'distance_min', 'distance_max'))) {
            $meta_query[] = array(
                'key' => $key,
                'value' => $value
            );
        }
    }
    if($searchArgs['price_max'] == 0){
        if($searchArgs['price_min'] > 0) {
            $meta_query[] = array(
                'key' => 'price',
                'value' => $searchArgs['price_min'],
                'type' => 'NUMERIC',
                'compare' => '>='
            );
        }
    }
    else{
        $meta_query[] = array(
            'key' => 'price',
            'value' => array($searchArgs['price_min'], $searchArgs['price_max']),
            'type' => 'NUMERIC',
            'compare' => 'BETWEEN'
        );
    }
    if($searchArgs['distance_max'] == 0){
        if($searchArgs['distance_min'] > 0) {
            $meta_query[] = array(
                'key' => 'distance',
                'value' => $searchArgs['distance_min'],
                'type' => 'NUMERIC',
                'compare' => '>='
            );
        }
    }
    else{
        $meta_query[] = array(
            'key' => 'distance',
            'value' => array($searchArgs['distance_min'], $searchArgs['distance_max']),
            'type' => 'NUMERIC',
            'compare' => 'BETWEEN'
        );
    }
    $args['meta_query'] = $meta_query;
}
$products = new WP_Query($args);
if($products->have_posts()) : ?>
<div class="recommended no_bg">
    <div class="col-w">
        <div class="block-title">
            <h3>新着車両情報</h3>
            <span>WHAT'S NEW</span>
        </div>
        <div class="list-item clearfix">
            <?php while($products->have_posts()) : $products->the_post(); ?>
                <?php get_template_part('includes/loop', 'product'); ?>
            <?php endwhile;wp_reset_postdata(); ?>
        </div>
        <?php if (function_exists("paginationSearch")) paginationSearch($products->max_num_pages, $paged, $searchArgs); ?>
    </div>
</div>
<?php else: ?>
    <p style="text-align: center;margin-top: 15px;">記事がありません</p>
<?php endif; ?>
<!-- End Recommended -->
<?php get_footer('hoan'); ?>