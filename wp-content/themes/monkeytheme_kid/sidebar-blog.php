<div id="side">
  <div class="sidead">
<?php if(is_mobile()) { ?>
<?php } else { ?>
<?php get_template_part('ad');?>
<?php } ?>
  </div>

<!-- popular posts -->
<?php if ( is_home() ): ?>
<div class="popular_blog sidebar_blog">
  <h4 class="menu_underh2">人気の記事</h4>
  <div class="sidebar_blog_in">
    <ul>
      <?php
      $args = array(
          'posts_per_page' => 5,
          'meta_key'       => 'post_views_count',
          'orderby'        => 'meta_value_num',
          'order'          => 'DESC',
      );
      $st_query = new WP_Query($args);
      ?>
      <?php if( $st_query->have_posts() ): ?>
          <?php while ($st_query->have_posts()) : $st_query->the_post(); ?>
          <li>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <div class="featured-image">
            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>
              <?php
                $title= get_the_title();
                the_post_thumbnail(array( 70,46 ),
                array( 'alt' =>$title, 'title' => $title)); 
              ?>
              <?php else: // サムネイルを持っていないときの処理 ?>
                <img src="<?php echo catch_that_image($post->post_content); ?>" width="70" height="46" />
              <?php endif; ?>
            </div>

            <div class="title"><?php the_title(); ?></div></a>
          </li>
          <?php endwhile; ?>
      <?php else: ?>
          <p>記事はありませんでした</p>
      <?php endif; ?>
      <?php wp_reset_postdata(); ?>

    </ul>
  </div>
</div>
<?php endif; ?>
<!-- /popular posts -->

<!-- recent posts -->
<?php if ( !is_home() ): ?>
<div class="newest_blog sidebar_blog">
  <h4 class="menu_underh2">最近の記事</h4>
  <div class="sidebar_blog_in">
    <ul>
      <?php
      $args = array(
          'posts_per_page' => 3,
      );
      $st_query = new WP_Query($args);
      ?>
      <?php if( $st_query->have_posts() ): ?>
          <?php while ($st_query->have_posts()) : $st_query->the_post(); ?>
          <li>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <div class="featured-image">
            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>
              <?php
                $title= get_the_title();
                the_post_thumbnail(array( 70,46 ),
                array( 'alt' =>$title, 'title' => $title)); 
              ?>
              <?php else: // サムネイルを持っていないときの処理 ?>
                <img src="<?php echo catch_that_image($post->post_content); ?>" width="70" height="46" />
              <?php endif; ?>
            </div>

            <div class="title"><?php the_title(); ?></div></a>
          </li>
          <?php endwhile; ?>
      <?php else: ?>
          <p>記事はありませんでした</p>
      <?php endif; ?>
      <?php wp_reset_postdata(); ?>

    </ul>
    <a class="view-more" title="view more" href="<?php echo home_url(); ?>/blog/recent-posts">もっと見る</a>
  </div>
</div>
<?php endif; ?>
<!-- /recent posts -->

<!-- ブログの読者登録＆解除ページ -->
<div class="smart_none">
  <div id="side_mail">
    <a href="<?php echo home_url();?>/blog/mail/" class="mail_btn">ブログの読者になる</a>
  </div>
</div>
<!-- /ブログの読者登録＆解除ページ -->

<!-- Search -->
<div class="smart_none">
  <div id="side_search">
    <h2 class="menu_underh2">キーワードから探す</h2>
    <div class="searchs">
      <?php get_search_form(); ?>
    </div>
  </div>
</div>
<!-- /Search -->

<!-- recommend posts -->
<?php if ( !is_home() ): ?>
<div class="recommend_blog sidebar_blog">
  <h4 class="menu_underh2">オススメの記事</h4>
  <div class="sidebar_blog_in">
    <ul>
      <?php
$categories = get_the_category($post->ID);
$category_ID = array();
foreach($categories as $category):
array_push( $category_ID, $category -> cat_ID);
endforeach ;
$args = array(
'posts_per_page'=> 5,
'category_name' => 'f-post',
'orderby' => 'rand',
);
$st_query = new WP_Query($args); ?>
          <?php
if( $st_query -> have_posts() ): ?>
          <?php
while ($st_query -> have_posts()) : $st_query -> the_post(); ?>
          <li>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <div class="featured-image">
            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>
              <?php
                $title= get_the_title();
                the_post_thumbnail(array( 70,46 ),
                array( 'alt' =>$title, 'title' => $title)); 
              ?>
              <?php else: // サムネイルを持っていないときの処理 ?>
                <img src="<?php echo catch_that_image($post->post_content); ?>" width="70" height="46" />
              <?php endif; ?>
            </div>

            <div class="title"><?php the_title(); ?></div></a>
          </li>
      <?php endwhile; else: ?>
          <p>記事はありませんでした</p>
      <?php endif; wp_reset_query(); ?>

    </ul>
  </div>
</div>
<?php endif; ?>
<!-- /recommend posts -->

<!-- SNS -->
<div class="kizi02"> 
  <!--SNSアカウント-->
  <div class="snsb-wrap">
    <h4 class="menu_underh2">公式SNSアカウント</h4>
      <ul class="snsb clearfix">
      <li style="padding-left:0;"><a href="" target="_blank"><img src="http://www.infact1.co.jp/share_img/side_tw.png" alt="twitter" width="50"></a></li>
      <li style="padding-left:0;"><a href="" target="_blank"><img src="http://www.infact1.co.jp/share_img/side_fb.png" alt="facebook" width="50"></a></li>
      <li style="padding-left:0;"><a href="" target="_blank"><img src="http://www.infact1.co.jp/share_img/side_google.png" alt="google" width="50"></a></li>
    </ul>
  </div>

  <div id="twibox">
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <?php endif; ?>
  </div>
</div>
<!-- /SNS -->

<!--アドセンス-->
<div id="ad1">
  <div style="text-align:center;">
    <?php get_template_part('scroll-ad');?>
  </div>
</div>

</div>
<!-- /#side -->