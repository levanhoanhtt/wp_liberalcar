<?php //require_once(__DIR__ . '/lib/Feed.php'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow" />
    <?php } ?>
    <title><?php wp_title(''); ?></title>
    <?php $theme_directory = get_stylesheet_directory_uri();?>
    <!--css切り替え-->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all"/>
    <link rel="stylesheet" href="<?php echo $theme_directory; ?>/assets/css/layout.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $theme_directory; ?>/assets/css/ngoc.css" type="text/css" media="all" />
    <style>
        #menu-navmenu li ul.nav-sub{top: 25px;}
        #menu-navmenu li ul.nav-sub li{width: 100%;}
        /*.cart-totalsupport .list-item .item h3.full{height: 52px;overflow: hidden;}*/
        #trust-form p#message-container-input{display: none;}
        body.page-template-6reason-after .box-maintaince img{width: 100%;max-height: 243px}
    </style>
    <link rel="stylesheet" href="<?php echo $theme_directory; ?>/assets/css/n-reponsive.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $theme_directory; ?>/assets/css/fix.css" type="text/css" media="all" />
    <?php wp_head(); ?>
    <meta name="google-site-verification" content="eOECF3RdLqPSnPhNQ9MCB3M4UCNw9F5b2mH2ThlUqrU"/>
</head>
<body <?php body_class(); ?>>
<div id="container">
    <div class="navig-wrap clearfix">
        <div id="navi-in" class="clearfix">
            <div class="clearfix">
                <div class="logo">
                    <p class="sitename">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo getMyOption('header_logo', $theme_directory.'/images/title.png'); ?>" alt="logo">
                        </a>
                        <span><?php echo getMyOption('header_text'); ?></span>
                    </p>
                </div>
                <div class="header-right">
                    <a class="headbtn02" href="#" target="_blank">
                        <img src="<?php echo $theme_directory; ?>/assets/img/home/btn_contact.png" alt="問い合わせ">
                    </a>
                </div>
                <div class="btn-menu" id="btn-menu">
                    <img src="<?php echo $theme_directory; ?>/assets/img/page03/icon17.png" alt=" " />
                </div>
            </div>
            <div id="navigation">
                <div class="navbar clearfix" >
                    <div class="menu-navmenu-container">
                        <?php $menu = wp_nav_menu(array('theme_location' => 'navbar', 'container' => '', 'echo' => false));
                        $menu = str_replace('menu-item-has-children', 'menu-item-has-children has-sub', $menu);
                        echo str_replace('sub-menu', 'sub-menu nav-sub', $menu); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>