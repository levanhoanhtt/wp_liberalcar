<?php get_header(); ?>



<div class="post archive-posts cat-posts  kizi"> 

  <!--ループ開始-->

  <h2 class="cat-title"> 

    <!--検索結果数--> 

    「<?php echo esc_html($s); ?>」の検索結果

<?php echo $wp_query->found_posts; ?>

    件 

    <!--検索結果数終わり--> 

  </h2>

  <div class="post-wrap clearfix">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article class="entry">
        <div class="entry-wrap clearfix">

          <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>

            <?php

            $title= get_the_title();

            the_post_thumbnail(array( 285,190 ),

            array( 'alt' =>$title, 'title' => $title)); ?>

            <?php else: // サムネイルを持っていないときの処理 ?>

            <img src="<?php echo catch_that_image($post->post_content); ?>" width="285" height="190" />

            <?php endif; ?>

            </a> </div>

          <!-- /.sumbox -->

          <div class="entry-content">

            <h3 class="entry-title"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

              <?php the_title(); ?>

              </a></h3>

          </div>

          <!-- .entry-content -->

        </div>

      </article>

      <!--/entry-->

  <?php endwhile; else: ?>

  <p>申し訳ありません。探している記事は現在ありません。</p>

  <?php endif; ?>

  </div>

  <div style="padding:20px 0px;">

    <?php get_template_part('ad');?>

  </div>

  <!--ページナビ-->

  <?php if (function_exists("pagination")) {

    pagination($wp_query->max_num_pages);

} ?>

  <!--ループ終了--> 

</div>

<!-- END div.post -->

<?php get_footer(); ?>

