<?php

/*-------------------------------------------*/
/*  Subscribe2のメールヘッダーを変更する
/*-------------------------------------------*/
function change_s2_mail_header(){
    $fromName = "WEBマーケティング";
    if ( function_exists('mb_encode_mimeheader') ) {
        $fromName = mb_encode_mimeheader($fromName);
    }
    $header['From'] = $fromName ." <". get_option('admin_email') .">";  //

    return $header;
}
add_filter('s2_email_headers','change_s2_mail_header');

function wptp_add_categories_to_attachments() {
      register_taxonomy_for_object_type( 'category', 'attachment' );  
}  
add_action( 'init' , 'wptp_add_categories_to_attachments' ); 



//カスタムヘッダー
$args = array(
	'width'         => 1180,
	'height'        => 300,
        'flex-height' => true,
	'default-image' => get_template_directory_uri() . '/images/stinger3.png',
);
add_theme_support( 'custom-header', $args );

//アイキャッチサムネイル
add_theme_support('post-thumbnails');
add_image_size('thumb101',101,67,true);
add_image_size('thumb285',285,190,true);
add_image_size('thumb70',70,46,true);

add_image_size('thumb100',100,100,true);
add_image_size('thumb110',110,110,true);
add_image_size('thumb150',150,110,true);
add_image_size('thumb210',210,140);
add_image_size('thumb250',250,250,true);


//ウィジェット
if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(5) )
 
if ( !function_exists('after_all') ):
function after_all() {
 
    register_sidebars(1,
        array(
        'name'=>'オススメ記事',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    register_sidebars(1,
        array(
        'name'=>'Footer Widget 1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    register_sidebars(1,
        array(
        'name'=>'Footer Widget 2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    register_sidebars(1,
        array(
        'name'=>'Footer Widget 3',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    register_sidebars(1,
        array(
        'name'=>'Footer Widget 4',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
 
    }
endif;
 
// 親テーマの後に実行
add_action( 'after_setup_theme', 'after_all' );


/**
* plugin all in one seo pack の keyword出力改造
*/
function my_keywords($keywords) {
    global $post;
    global $aioseop_options;
 
    if ( get_option( 'show_on_front' ) == 'page' && $post->ID == get_option( 'page_on_front' ) && get_post_meta( $post->ID, "_aioseop_keywords", true )) {
        $keywords = esc_html(stripcslashes(get_post_meta( $post->ID, "_aioseop_keywords", true ) ) );
    } else {
        $keywords = trim($keywords);
        if (!$keywords) {
            $keywords = esc_html(trim($aioseop_options['aiosp_home_keywords']));
        }
    }
    return $keywords;
}
add_filter('aioseop_keywords', 'my_keywords');

/**
* all in one seo pack の description出力改造
*/
function my_description($description) {
    global $post;
    global $aioseop_options;
 
    if (trim($description)) {
        return trim($description);
    }
 
    /* default */
    $description = $aioseop_options['aiosp_home_description'];
 
    if ((is_archive()) || (is_home())) {
        /* blog */
        $description = 'お気に入りの記事がありましたら、シェアをお願いします。';
    } elseif(is_single() || is_page()) {
        /* single or page */
        $description = get_post_field('post_content', $post->ID);
    }
 
    $description = strip_shortcodes( $description );
    $description = str_replace( ']]>', ']]&gt;', $description );
    $description = strip_tags( $description );
    $description = str_replace( "\xc2\xa0", " ", $description );
    $description = trim( preg_replace( '/[\n\r\t ]+/', ' ', $description), ' ' );
    $description = mb_convert_kana($description,'sKV','UTF-8');
    $description = mb_substr( $description, 0, 120, 'UTF-8' );
    $_find_pos = mb_strrpos($description,'。',0,'UTF-8');
    if (($_find_pos >= 1) && ($_find_pos <= 120 - 1)) {
        $description = mb_substr( $description, 0, $_find_pos + 1, 'UTF-8' );
    }
 
    return $description;
}
add_filter('aioseop_description', 'my_description');


/* 記事一番最初の画像をアイキャッチに自動で入れる */
function catch_that_image($content) {
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
    $first_img = $matches [1] [0];
  
    if(empty($first_img)){ //Defines a default image
        $first_img = get_stylesheet_directory_uri()."/images/no-img.png";
    }
return $first_img;
}

/* メールヘッダーを変更する */
add_filter( 'wp_mail_from', 'change_mail_from' );
add_filter( 'wp_mail_from_name', 'change_mail_from_name' );

function change_mail_from($from_mail) {
return 'info@infact1.co.jp';
}
function change_mail_from_name($from_name) {
return '株式会社インファクト';
}

/*-------------------------------------------*/
/*  本体のアップデート通知を非表示
/*-------------------------------------------*/
add_filter('pre_site_transient_update_core', create_function('$a', "return  null;"));
/*-------------------------------------------*/
/*  プラグイン更新通知を非表示
/*-------------------------------------------*/
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
/*-------------------------------------------*/
/*  テーマ更新通知を非表示
/*-------------------------------------------*/
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );

// Breadcrumbs
function custom_breadcrumbs() {
       
    // Settings
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'TOP';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><span class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</span></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><span class="bread-current bread-archive">' . $custom_tax_name . '</span></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
         
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><span class="bread-current bread-cat">' . single_cat_title('', false) . '</span></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';

                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><span class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</span></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><span class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
    
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</span></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><span class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}

//カテゴリー説明文でHTMLタグを使う
remove_filter( 'pre_term_description', 'wp_filter_kses' );
//カテゴリー説明文からPタグを除去
remove_filter('term_description', 'wpautop');

// Breadblog
function custom_breadblog() {
       
    // Settings
    $breadblog_id      = 'breadcrumbs';
    $breadblog_class   = 'breadcrumbs';
    $home_title         = 'BLOGTOP';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul class="' . $breadblog_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '/blog/" title="' . $home_title . '">' . $home_title . '</a></li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><span class="bread-current bread-archive">' . post_type_archive_title('', false) . '</span></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><span class="bread-current bread-archive">' . $custom_tax_name . '</span></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
         
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><span class="bread-current bread-cat">' . single_cat_title('', false) . '</span></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);

                $parents = '';
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';

                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><span class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</span></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><span class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
    
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</span></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><span class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}

//カテゴリー説明文でHTMLタグを使う
remove_filter( 'pre_term_description', 'wp_filter_kses' );
//カテゴリー説明文からPタグを除去
remove_filter('term_description', 'wpautop');

/***********************************************************
* 投稿画面の項目を非表示
***********************************************************/
function remove_default_post_screen_metaboxes() {
remove_meta_box( 'postcustom','post','normal' ); // カスタムフィールド
remove_meta_box( 'postexcerpt','post','normal' ); // 抜粋// Breadcrumbs
remove_meta_box( 'commentstatusdiv','post','normal' ); // ディスカッション
remove_meta_box( 'commentsdiv','post','normal' ); // コメント
remove_meta_box( 'trackbacksdiv','post','normal' ); // トラックバック
remove_meta_box( 'slugdiv','post','normal' ); // スラッグ
}
add_action('admin_menu','remove_default_post_screen_metaboxes');
/*-------------------------------------------*/
/*  検索結果から固定ページを除外
/*-------------------------------------------*/
function SearchFilter($query) {
    if ( !is_admin() && $query->is_main_query() && $query->is_search() ) {
        $query->set('post_type', 'post');
    }
    return $query;
}
add_filter('pre_get_posts','SearchFilter');

/*-------------------------------------------*/
/*  アイキャッチ下にバナー表示
/*-------------------------------------------*/
// add_filter('the_content', 'cntAd');
// function cntAd($content){    
//     $ad = '<!--RndAds-->';
//     $count = 0;
     
//     if(is_single()){
//             $arrayCnts = preg_split('/<\/p>/', $content, -1, PREG_SPLIT_NO_EMPTY);
//             foreach( $arrayCnts as $arrayCnt ){
//                 $count++;
//                 if($count == 1){
//                 $arrayCntAd[] = $arrayCnt.'</p>'.$ad;
//                 } else {
//                 $arrayCntAd[] = $arrayCnt.'</p>';
//                 }
//             }        
//             $content = implode("", $arrayCntAd);
//     }
//     return $content;
// }

/*-------------------------------------------*/
/*  公開確認
/*-------------------------------------------*/
$c_message = '本当にしますよ？よろしいですね？？？ね？';
function confirm_publish(){
/*-------------------------------------------*/
/*  JavaScriptを管理画面フッターに挿入する
/*-------------------------------------------*/
global $c_message;
echo '<script type="text/javascript"><!--
var publish = document.getElementById("publish");
if (publish !== null) publish.onclick = function(){
return confirm("'.$c_message.'");
};
// --></script>';
}
//add_action('admin_footer', 'confirm_publish');
/*-------------------------------------------*/
/*  投稿画面CSS&js追加
/*-------------------------------------------*/
function custom_enqueue($hook_suffix) {
    // 新規投稿または編集画面のみ
    if( 'post.php' == $hook_suffix || 'post-new.php' == $hook_suffix ) {
        // 読み込むスクリプトファイル(※依存関係:jquery)
        wp_enqueue_script('custom_js', get_stylesheet_directory_uri() . '/jsapi.js', array('jquery'));
        // 読み込むスクリプトファイル(※依存関係:jquery)
        wp_enqueue_script('custom_js2', get_stylesheet_directory_uri() . '/admin.js', array('jquery'));
        // 読み込むCSSファイル
        wp_enqueue_style('custom_css', get_stylesheet_directory_uri() . '/admin-style.css');
    }
}
// "custom_enqueue" 関数を管理画面のキューアクションにフック
add_action( 'admin_enqueue_scripts', 'custom_enqueue' );
/*-------------------------------------------*/
/*  投稿画面　アナウンス
/*-------------------------------------------*/
function caution_title_head(){
     echo '<div id="feed1"></div>';
}
add_action('edit_form_top', 'caution_title_head');

/*-------------------------------------------*/
/*  Get views of post
/*-------------------------------------------*/
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count ==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

/*-------------------------------------------*/
/*  Set views for post
/*-------------------------------------------*/
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count ==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
/*-------------------------------------------*/
/*  空文字検索機能
/*-------------------------------------------*/
function custom_search($search, $wp_query  ) {
    //query['s']があったら検索ページ表示
    if ( isset($wp_query->query['s']) ) $wp_query->is_search = true;
    return $search;
}
add_filter('posts_search','custom_search', 10, 2);
// テンプレート読み込みフィルターをカスタマイズ
add_filter('template_include','custom_search_template');
function custom_search_template($template){
    // 検索結果の時
    if ( is_search() ) {
        // 表示する投稿タイプを取得
        $post_types = get_query_var('post_type');
        // search-{$post_type}.php の読み込みルールを追加
        foreach ( (array) $post_types as $post_type )
            $templates[] = "search-{$post_type}.php";
        $templates[] = 'search.php';
        $template = get_query_template('search',$templates);
    }
    return $template;
}
/**
 * Add custom node to Feed output
 */
function add_my_rss_node() {
    global $post;
    $tags = serialize(wp_get_post_tags($post->ID));
    echo "<tags>{$tags}</tags>";
}
add_action('rss2_item', 'add_my_rss_node');

/**
 * Check string is tag or not
 */
function in_tags($tags = array(), $str = null) {
    foreach ($tags as $tag) {
        if ($str === $tag->name) {
            return true;
        }
    }
    return false;
}

/**
 * Get prefix by tags
 */
function get_prefix($tags = array()) {
    switch (true) {
        case in_tags($tags, '重要'):
            $prefix = '<img src="' . get_stylesheet_directory_uri() . '/images/news_important.gif">';
            break;
        case in_tags($tags, 'セミナー'):
            $prefix = '<img src="' . get_stylesheet_directory_uri() . '/images/news_seminar.gif">';
            break;
        default:
            $prefix = '<img src="' . get_stylesheet_directory_uri() . '/images/news_news.gif">';
            break;
    }
    return $prefix;
}

/**
 * create shortcode to display feed
 */ 
function create_shortcode_news_feed($args) {
    $rss_news = Feed::loadRss('http://www.infact1.co.jp/staff_blog/category/news/feed/');
    $limit = isset($args['limit']) ? (int)$args['limit'] : 8;
    $count = 0;
    ob_start(); ?>
      <div id ="newsRss">
        <?php foreach ($rss_news->item as $item): ?>
          <?php if ($count >= $limit) break; $count++; ?>
          <div class="newsRssBox">
            <div><?php echo get_prefix(unserialize($item->tags)); ?></div><div><?php echo date('Y.m.d', strtotime($item->pubDate)) ?></div><h4><a href="<?php echo htmlSpecialChars($item->link) ?>" title="<?php echo htmlSpecialChars($item->title) ?>" target="_blank"><?php echo htmlSpecialChars($item->title) ?></a></h4>
          </div>
        <?php endforeach ?>
      </div>
    <?php
    $html = ob_get_contents();

    ob_end_clean();

    return $html;
}

add_shortcode('sc_news_feed', 'create_shortcode_news_feed');

class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );
  
  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul>\n";
  }
  
  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }
  
  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
  
    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = ''; 
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    
    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }
    
    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
    }
    
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
    
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
    
    $output .= $indent . '<li' . $id . $value . $class_names .'>';
    
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    
    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'><span>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</span></a>';
    $item_output .= $args->after;
    
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
  
  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}

function get_the_slug() {

    global $post;
    $slug = null;

    if (is_single() || is_page()) {
        $slug = $post->post_name;
    } elseif (is_category()) {
        $cat = get_query_var('cat');
        $yourcat = get_category ($cat);
        $slug = $yourcat->slug;
    }
    return $slug;
}

/**
 * create shortcode to display recommend feed
 */ 
function create_shortcode_recommend_feed($args) {
    $rss_news = Feed::loadRss('http://www.infact1.co.jp/staff_blog/category/f-post/feed/');
    $limit = isset($args['limit']) ? (int)$args['limit'] : 8;
    $count = 0;
    ob_start(); ?>
        <?php foreach ($rss_news->item as $item): ?>
          <?php if ($count >= $limit) break; $count++; ?>
            <article class="entry">
                <div class="entry-wrap clearfix">
                    <div class="sumbox">
                      <a href="<?php echo htmlSpecialChars($item->link) ?>" title="<?php echo htmlSpecialChars($item->title) ?>" target="_blank">
                        <?php preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $item->{'content:encoded'}, $image); ?>
                        <img src="<?php echo $image[1]; ?>" class="wp-post-image" alt="">
                      </a>
                    </div>
                    <!-- /.sumbox -->
                    <div class="entry-content">
                        <a href="<?php echo htmlSpecialChars($item->link) ?>" title="<?php echo htmlSpecialChars($item->title) ?>" target="_blank">
                            <?php $title= mb_substr($item->title,0,52); echo $title; ?>
                        </a>
                    </div>
                    <!-- .entry-content -->
                </div>
            </article>
        <?php endforeach ?>
    <?php
    $html = ob_get_contents();

    ob_end_clean();

    return $html;
}

add_shortcode('sc_recommend_feed', 'create_shortcode_recommend_feed');

/*Hoan Mua Da*/
//add_excerpts_to_pages
add_action('init', 'my_add_excerpts_to_pages');
function my_add_excerpts_to_pages(){
    add_post_type_support('page', 'excerpt');
}
// create menu support for theme.
if (function_exists('register_nav_menus')) {
    register_nav_menus(
        array(
            'footer_nav_1' => 'フッターナビゲーションバー',
            'footer_nav_2' => '会社案内'
        )
    );
}
//theme option
if( function_exists('acf_add_options_page') ) {
    $args = array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => '',
        'capability' => 'edit_posts',
        'position' => false,
        'parent_slug' => '',
        'icon_url' => false,
        'redirect' => true,
        'post_id' => 'theme_options',
        'autoload' => false
    );
    acf_add_options_page($args);
}
if ( !is_admin() ) {
    global $myOptions;
    $myOptions = array();
    $fields = (array)get_field_objects('theme_options');
    foreach( $fields as $label => $field ){
        if($field['type'] == 'image'){
            $p = get_post($field['value']);
            if($p) $myOptions[$label] = $p->guid;
        }
        else $myOptions[$label] = $field['value'];
    }
}
function getMyOption($optionName, $defaultValue = ""){
    if(!empty($optionName)){
        global $myOptions;
        if($myOptions){
            if(isset($myOptions[$optionName])) return $myOptions[$optionName];
            else{
                $value = get_field($optionName, 'theme_options');
                if(!$value) $value = $defaultValue;
                return $value;
            }
        }
        else{
            $value = get_field($optionName, 'theme_options');
            if(!$value) $value = $defaultValue;
            return $value;
        }
    }
    return $defaultValue;
}
function getObjectValue($listObjs, $label, $defaultValue = ""){
    if(isset($listObjs[$label]) && !empty($listObjs[$label])) return $listObjs[$label];
    return $defaultValue;
}
function paginationSearch($pages = 1, $paged = 1, $searchArgs = array(), $range = 4){
    $showitems = ($range * 2) + 1;//9
    if($pages > 1){
        $linkBase = '/search?';
        //$paged = (isset($_GET['pg']) && $_GET['pg'] > 0) ? $_GET['pg'] : 1;
        if(!empty($searchArgs)){
            foreach($searchArgs as $label => $value){
                if(!empty($value)) $linkBase .= $label . '=' . $value . '&';
            }
        }
        $linkBase .= 'pg=';
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".$linkBase."1'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".$linkBase . ($paged - 1)."'>&lsaquo; Previous</a>";
        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems)){
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".$linkBase . $i ."' class=\"inactive\">".$i."</a>";
            }
        }
        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".$linkBase . ($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".$linkBase . ($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}
function paginationVoice($pages = 1, $paged = 1, $shopId = 0, $range = 4){
    $showitems = ($range * 2) + 1;//9
    if($pages > 1){
        if($shopId > 0) $linkBase = "/voice?shopId={$shopId}&pg=";
        else $linkBase = '/voice?pg=';
        echo '<div class="n-pager"><ul>';
        if($paged > 1 && $showitems < $pages) echo '<li><a class="prev" href="'.$linkBase . ($paged - 1).'">Prev</a></li>';
        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems)){
                echo ($paged == $i)? '<li><a href="javascript:void(0)" class="active">'.$i.'</a></li>' : '<li><a href="'.$linkBase . $i.'">'.$i.'</a></li>';
            }
        }
        if ($paged < $pages && $showitems < $pages) echo '<li><a class="next" href="'.$linkBase . ($paged + 1).'">Next</a></li>';
        echo "</ul></div>\n";
    }
}
//custom post type
//faq
function custom_post_faq(){
    $labels = array(
        'name' => __('よくある質問'),
        'singular_name' => __('よくある質問'),
        'add_new' => __('Add New よくある質問'),
        'add_new_item' => __('Add New よくある質問'),
        'edit_item' => __('Edit'),
        'new_item' => __('Add New よくある質問'),
        'all_items' => __('All よくある質問'),
        'view_item' => __('View よくある質問'),
        'search_items' => __('Search よくある質問'),
        'not_found' => __('Not found よくある質問'),
        'not_found_in_trash' => __('Not found よくある質問'),
        'parent_item_colon' => '',
        'menu_name' => 'よくある質問'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'よくある質問',
        'public' => true,
        'menu_position' => 4,
        'supports' => array('title', 'editor', 'revisions'),
        'has_archive' => true,
        'rewrite' => array('slug' => 'faq', 'with_front' => false),
        'show_in_nav_menus' => true
    );
    register_post_type('faq', $args);
}
add_action('init', 'custom_post_faq');

function create_faq_taxonomies(){
    $labels = array(
        'name' => __('カテゴリ'),
        'singular_name' => __('カテゴリ'),
        'search_items' => __('Search カテゴリ'),
        'all_items' => __('All カテゴリ'),
        'parent_item' => __('Parent カテゴリ'),
        'parent_item_colon' => __('Parent カテゴリ'),
        'edit_item' => __('Edit'),
        'update_item' => __('Update'),
        'add_new_item' => __('Add New'),
        'new_item_name' => __('Add New'),
        'menu_name' => __('カテゴリ')
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'faq_model', 'with_front' => false),
        'show_in_menu' => true,
        'show_in_nav_menus' => true
    );
    register_taxonomy('faq_model', array('faq'), $args);
}
add_action('init', 'create_faq_taxonomies', 0);

//shop
function custom_post_shop(){
    $labels = array(
        'name' => __('店舗案内'),
        'singular_name' => __('店舗案内'),
        'add_new' => __('Add New 店舗案内'),
        'add_new_item' => __('Add New 店舗案内'),
        'edit_item' => __('Edit'),
        'new_item' => __('Add New 店舗案内'),
        'all_items' => __('All 店舗案内'),
        'view_item' => __('View 店舗案内'),
        'search_items' => __('Search 店舗案内'),
        'not_found' => __('Not found 店舗案内'),
        'not_found_in_trash' => __('Not found 店舗案内'),
        'parent_item_colon' => '',
        'menu_name' => '店舗案内'
    );
    $args = array(
        'labels' => $labels,
        'description' => '店舗案内',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
        'has_archive' => false,
        'rewrite' => array('slug' => 'shop', 'with_front' => false),
        'show_in_nav_menus' => true
    );
    register_post_type('shop', $args);
}
add_action('init', 'custom_post_shop');

//customer voice
function custom_post_customer_voice(){
    $labels = array(
        'name' => __('お客様の声'),
        'singular_name' => __('お客様の声'),
        'add_new' => __('Add New お客様の声'),
        'add_new_item' => __('Add New お客様の声'),
        'edit_item' => __('Edit'),
        'new_item' => __('Add New お客様の声'),
        'all_items' => __('All お客様の声'),
        'view_item' => __('View お客様の声'),
        'search_items' => __('Search お客様の声'),
        'not_found' => __('Not found お客様の声'),
        'not_found_in_trash' => __('Not found お客様の声'),
        'parent_item_colon' => '',
        'menu_name' => 'お客様の声'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'お客様の声',
        'public' => true,
        'menu_position' => 6,
        'supports' => array('title', 'editor'),
        'has_archive' => false,
        'rewrite' => array('slug' => 'customer_voice', 'with_front' => false),
        'show_in_nav_menus' => true
    );
    register_post_type('customer_voice', $args);
}
add_action('init', 'custom_post_customer_voice');

/*function custom_post_search(){
    $labels = array(
        'name' => __('車在庫'),
        'singular_name' => __('車在庫'),
        'add_new' => __('Add New 車在庫'),
        'add_new_item' => __('Add New 車在庫'),
        'edit_item' => __('Edit'),
        'new_item' => __('Add New 車在庫'),
        'all_items' => __('All 車在庫'),
        'view_item' => __('View 車在庫'),
        'search_items' => __('Search P車在庫'),
        'not_found' => __('Not found 車在庫'),
        'not_found_in_trash' => __('Not found 車在庫'),
        'parent_item_colon' => '',
        'menu_name' => '車在庫'
    );
    $args = array(
        'labels' => $labels,
        'description' => '車在庫',
        'public' => true,
        'menu_position' => 4,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
        'has_archive' => true,
        'rewrite' => array('slug' => 'search', 'with_front' => false),
        'show_in_nav_menus' => true
    );
    register_post_type('search', $args);
}
add_action('init', 'custom_post_search');

function create_search_taxonomies(){
    $labels = array(
        'name' => __('車種'),
        'singular_name' => __('車種'),
        'search_items' => __('Search 車種'),
        'all_items' => __('All 車種'),
        'parent_item' => __('Parent 車種'),
        'parent_item_colon' => __('Parent 車種'),
        'edit_item' => __('Edit'),
        'update_item' => __('Update'),
        'add_new_item' => __('Add New'),
        'new_item_name' => __('Add New'),
        'menu_name' => __('車種')
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'search', 'with_front' => false),
        'show_in_menu' => true,
        'show_in_nav_menus' => true
    );
    register_taxonomy('search_model', array('search'), $args);
}
add_action('init', 'create_search_taxonomies', 0);*/