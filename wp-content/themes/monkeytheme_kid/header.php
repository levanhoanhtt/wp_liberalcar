<?php
// require lib to parse RSS
require_once(__DIR__ . '/lib/Feed.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <?php if (is_category()): ?>
    <?php elseif (is_archive()): ?>
        <meta name="robots" content="noindex">
    <?php elseif (is_tag()): ?>
        <meta name="robots" content="noindex">
    <?php endif; ?>
    <title>
        <?php
        global $page, $paged;
        if (is_front_page()):
            bloginfo('name');
        elseif (is_single()):
            wp_title('');
        elseif (is_page()):
            wp_title('');
        elseif (is_archive()):
            wp_title('|', true, 'right');
            bloginfo('name');
        elseif (is_search()):
            wp_title('-', true, 'right');
        elseif (is_404()):
            echo '404 - ';
            bloginfo('name');
        endif;
        if ($paged >= 2 || $page >= 2):
            echo '-' . sprintf('%sページ',
                    max($paged, $page));
        endif;
        ?>
    </title>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed"
          href="<?php bloginfo('rss2_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="shortcut icon" href="<?php echo home_url(); ?>/favicon.ico"/>
    <!-- ここからOGP -->
    <meta property="og:type"
          content="<?php if ($_SERVER["REQUEST_URI"] == "/") { ?>blog<?php } else { ?>article<?php } ?>">
    <?php
    if (is_single()) {//単一記事ページの場合
        if (have_posts()): while (have_posts()): the_post();
            echo '<meta property="og:description" content="' . get_post_meta($post->ID, '_aioseop_description', true) . '">';
            echo "\n";//抜粋を表示
        endwhile; endif;
        echo '<meta property="og:title" content="';
        the_title();
        echo '">';
        echo "\n";//単一記事タイトルを表示
        echo '<meta property="og:url" content="';
        the_permalink();
        echo '">';
        echo "\n";//単一記事URLを表示
    } else {//単一記事ページページ以外の場合（アーカイブページやホームなど）
        echo '<meta property="og:description" content="';
        bloginfo('description');
        echo '">';
        echo "\n";//「一般設定」管理画面で指定したブログの説明文を表示
        echo '<meta property="og:title" content="';
        bloginfo('name');
        echo '">';
        echo "\n";//「一般設定」管理画面で指定したブログのタイトルを表示
        echo '<meta property="og:url" content="';
        echo home_url('/');
        echo '">';
        echo "\n";//「一般設定」管理画面で指定したブログのURLを表示
    }
    ?>
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
    <?php
    $str = $post->post_content;
    $searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';//投稿にイメージがあるか調べる
    if (is_single()) {//単一記事ページの場合
        if (has_post_thumbnail()) {//投稿にサムネイルがある場合の処理
            $image_id = get_post_thumbnail_id();
            $image = wp_get_attachment_image_src($image_id, 'full');
            echo '<meta property="og:image" content="' . $image[0] . '">';
            echo "\n";
        } else if (preg_match($searchPattern, $str, $imgurl) && !is_archive()) {//投稿にサムネイルは無いが画像がある場合の処理
            echo '<meta property="og:image" content="' . $imgurl[2] . '">';
            echo "\n";
        } else {//投稿にサムネイルも画像も無い場合の処理
            echo '<meta property="og:image" content="';
            echo home_url('/ogp.jpg');
            echo '">';
            echo "\n";
        }
    } else {//単一記事ページページ以外の場合（アーカイブページやホームなど）
        echo '<meta property="og:image" content="';
        echo home_url('/ogp.jpg');
        echo '">';
        echo "\n";
    }
    ?>
    <?php if (is_home()): ?>  <!-- ←でブログのトップページを判定 -->
        <meta name="twitter:description" content="<?php bloginfo('description'); ?>">
    <?php elseif (is_page()): ?> <!-- ←で固定ページを判定 -->
        <meta name="twitter:description" content="<?php bloginfo('description'); ?>">
    <?php else: ?> <!-- ←上記の条件にもれたページ（記事ページ） -->
        <meta name="twitter:description"
              content="<?php echo mb_substr(str_replace(array("rn", "r", "n"), '', strip_tags($post->post_content)), 0, 100) . '...'; ?>">
    <?php endif; ?>
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
    <meta property="og:locale" content="ja_JP"/>
    <meta property="fb:app_id" content="FBのアプリIDを入れて下さい">
    <meta name="twitter:card" content="summary_large_image">
    <!-- ここまでOGP -->
    <!--css切り替え-->
    <?php if (is_mobile()) { ?>
        <link rel="apple-touch-icon-precomposed"
              href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon-precomposed.png"/>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all"/>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/smart.css" type="text/css"
              media="all"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <?php } else { ?>
        <meta name="viewport" content="width=1024, maximum-scale=1, user-scalable=yes">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all"/>
    <?php } ?>
    <?php wp_head(); ?>
    <script type='text/javascript'
            src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js?ver=4.3.1'></script>
    <!--エバーノート-->
    <script type="text/javascript" src="http://static.evernote.com/noteit.js"></script>
    <!-- アナリティクス -->

    <!-- webマスター -->
    <meta name="google-site-verification" content="eOECF3RdLqPSnPhNQ9MCB3M4UCNw9F5b2mH2ThlUqrU"/>

    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/breakpoints.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/img_change.js"></script>
    <?php if (is_mobile()) { ?>
        <!-- スライドメニュー -->
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/sp_slidemenu.js"></script>
    <?php } ?>
    <!-- other scripts -->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/script.js"></script>
    <!--ブログ-->
    <?php
    $url = $_SERVER['REQUEST_URI'];
    if (strstr($url, 'blog') == true):
        ?>
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/nav_fix.js"></script>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/blog.css" type="text/css" media="all"/>
    <?php if (is_mobile()): ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/blog_sp.css" type="text/css"
          media="all"/>
    <?php endif; ?>
    <?php endif; ?>
</head>


<body <?php body_class(); ?>>
<!--FB-->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=アプリID&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--endFB-->
<?php if (is_mobile()) { ?>
    <?php
} else {
    ?>
    <?php
}
?>

<div id="container">
    <!-- /#header -->

    <?php if (is_mobile()) { ?>
        <?php
    } else {
        ?>

        <?php
    }
    ?>
    <!--pcnavi-->
    <div class="navig-wrap clearfix">
        <div id="navi-in" class="clearfix">
            <div class="clearfix">
                <div class="logo">
                    <?php if (is_front_page()): ?>
                        <h1 class="sitename">
                            <a href="<?php echo home_url(); ?>/">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title.png" alt="logo">
                            </a><span>充実のトータルカーサービス。クルマのことは全てお任せください。</span>
                        </h1>
                    <?php else: ?>
                        <p class="sitename">
                            <a href="<?php echo home_url(); ?>/">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title.png" alt="logo">
                            </a><span>充実のトータルカーサービス。クルマのことは全てお任せください。</span>
                        </p>
                    <?php endif; ?>
                </div>
                <?php if (!is_mobile()): ?>
                <div class="header-right">
                    <a class="headbtn01" href="" target="_blank"><img
                            src="<?php echo get_stylesheet_directory_uri(); ?>/images/button/btn_recruit.png"
                            alt="採用情報"></a>
                    <a class="headbtn02" href="" target="_blank"><img
                            src="<?php echo get_stylesheet_directory_uri(); ?>/images/button/btn_contact.png"
                            alt="問い合わせ"></a>
                </div>
            </div>
            <div class="navbar clearfix">
                <?php wp_nav_menu(array('theme_location' => 'navbar')); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--/pcnavi-->
    <?php
    $url = $_SERVER['REQUEST_URI'];
    if (strstr($url, 'blog') == true):
        ?><?php if (!is_mobile()): ?>
        <nav
            class="smanone clearfix"><?php wp_nav_menu(array('theme_location' => 'navbar', 'menu' => "blog-menu")); ?></nav>
    <?php endif; ?><?php endif; ?>
    <!--
  <?php if (is_page()) : ?>
    <div class="page-feature-image">
      <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post_id)); ?>" alt="featured">
    </div>
  <?php endif; ?>
  -->
    <div class="page-feature-image">
        <?php switch (get_the_slug()): ?>
<?php case 'test': ?>
                <img class="sp-img" src="<?php echo get_stylesheet_directory_uri(); ?>/images/banner/page_pc.jpg"
                     alt="page">
                <?php break; ?>
            <?php endswitch; ?>

    </div>

    <div class="rev-wrap">
        <div class="rev-in">
            <?php if (is_mobile()) { ?>
                <?php putRevSlider("sp_revslider", "homepage"); ?>
            <?php } else { ?>
                <?php putRevSlider("revslider", "homepage"); ?>
            <?php } ?>
            <?php if (is_category()) : ?>
                <?php if (wp_is_mobile()): ?>
                    <?php
                    global $wp_query;
                    $cat_obj = $wp_query->get_queried_object();
                    if ($cat_obj) {
                        $current_category = $cat_obj->slug;
                        $args = array(
                            'post_status' => 'inherit',
                            'post_type' => 'attachment',
                            'category_name' => $current_category,
                        );
                        $queryimg = new WP_Query($args);
                    }

                    if ($queryimg->post->guid) {
                        echo '<img src="';
                        echo $queryimg->post->guid; //url to the first attachment in the query.
                        echo '" />';
                    }

                    wp_reset_postdata();
                    ?>
                <?php else : ?>
                    <?php if (function_exists('z_taxonomy_image')) : ?>
                        <?php z_taxonomy_image(); ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
            $url = $_SERVER['REQUEST_URI'];
            if (strstr($url, 'blog') == true):
                ?>
                <?php if (is_home()): ?><?php if (get_header_image()): ?>
                <div id="gazou">
                    <div id="gazou-in">
                        <p id="headimg"><img src="<?php header_image(); ?>" alt="*"
                                             width="<?php echo HEADER_IMAGE_WIDTH; ?>"
                                             height="<?php echo HEADER_IMAGE_HEIGHT; ?>"/></p>
                    </div>
                </div>
            <?php endif; ?>
            <?php endif; ?><?php endif; ?>
        </div>
    </div>


    <div id="wrap">
        <!--オススメ記事表示-->

        <!--/オススメ記事表示-->
        <div id="wrap-in">
            <?php if (!is_home()) : ?>
                <?php
                $url = $_SERVER['REQUEST_URI'];
                if (strstr($url, 'blog') == true):
                    ?>
                    <div id="breadcrumb"><?php custom_breadblog(); ?>  </div>
                <?php else : ?>
                    <div
                        id="breadcrumb"><?php custom_breadcrumbs(); ?>  </div><?php endif; ?><!--/breadcrumb--><?php endif; ?>
            <?php if (is_home()) : ?>
            <?php elseif (is_category()) : ?>
            <?php elseif (is_page(array(42, 44))) : ?>
            <?php else: ?>
            <div id="top_con">
                <?php endif; ?>
                <div id="main">