<?php /* Template Name: トップページ */ ?>

<?php get_header(); ?>
<div class="post cat-posts news">
  <h3 class="cat-name">インファクトからのお知らせ</h3>
  <?php echo do_shortcode('[sc_news_feed limit="8"]');?>
  <div id="news_btn">
    <a href="http://www.infact1.co.jp/staff_blog/category/news/" class="news_btn" target="_blank">お知らせ一覧はコチラ</a>
  </div>
</div>

<div class="post cat-posts cat-posts-1">
  <!--ループ開始-->
  <h3 class="cat-name">インファクトができること</h3>
  <div class="post-wrap clearfix">
    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/proceeds/media_ka">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat1_img01.jpg" class="wp-post-image" alt="インファクトができること">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="entry-title">
            <a href="/proceeds/media_ka">企業のメディア化</a>
          </h3>
          <p>「広告費をかけずに自社で集客力をアップさせたい」企業のメディア化で集客力・営業力が劇的に向上！</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/proceeds/original_hp">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat1_img02.jpg" class="wp-post-image" alt="ホームページ制作">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="entry-title">
            <a href="/proceeds/original_hp">ホームページ制作</a>
          </h3>
          <p>「企業の顔」コーポレートサイトから売上アップに直結するホームページまで。集客のプロにお任せ！</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/proceeds/training">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat1_img03.jpg" class="wp-post-image" alt="企業のWEBとSNS活用診断＆研修">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="entry-title">
            <a href="/proceeds/consulting">企業のWEBとSNS活用診断＆研修</a>
          </h3>
          <p>「ホームページからの反応が無い」「SNSを上手に活用できない」そんな企業のお悩み解決します！</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/proceeds/sp_top">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat1_img04.jpg" class="wp-post-image" alt="スマホ検索がＰＣ検索を逆転？！未対策の企業は今すぐ対応を！！">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="entry-title">
            <a href="/proceeds/sp_top">スマホサイト対応</a>
          </h3>
          <p>2016年にはスマホ検索がPC検索を逆転？！ホームページのスマホ対策はもはや必須です！</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

  </div><!--post-wrap-->
  <!--ループ終了--> 
</div>

<div class="post cat-posts cat-posts-2">
  <!--ループ開始-->
  <h3 class="cat-name">インファクトにご依頼頂く前に必ずご覧くださいませ</h3>
  <div class="post-wrap clearfix">

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/flow">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat2_img01.jpg" class="wp-post-image" alt="ご依頼から制作までの流れ">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="request-title">
            <a href="/flow">ご依頼から制作までの流れ</a>
          </h3>
            <p>インファクトでホームページ制作を承る場合のサービスの流れを解説。ご依頼前に、ご確認ください。</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/jisseki">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat2_img02.jpg" class="wp-post-image" alt="制作実績">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="request-title">
            <a href="/jisseki">制作実績</a>
          </h3>
            <p>これまでインファクトが手がけたホームページ制作・コンサルティング実績の一部を紹介いたします。</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

    <article class="entry">
      <div class="entry-wrap clearfix">
        <div class="sumbox">
          <a href="/voice">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cat2_img03.jpg" class="wp-post-image" alt="お客様の声">
          </a>
        </div>
        <!-- /.sumbox -->
        <div class="entry-content">
          <h3 class="request-title">
            <a href="/voice">お客様の声</a>
          </h3>
            <p>インファクトに寄せられたお客様の声。その一部をご紹介いたします。どうぞ参考にしてください。</p>
        </div>
        <!-- .entry-content -->
      </div>
    </article>

  </div><!--post-wrap-->
</div>

<div class="post cat-posts cat-posts-3">
  <h3 class="cat-name">インファクトのオススメブログ記事</h3>
  <div id="rss-blog" class="post-wrap clearfix">
      <?php echo do_shortcode('[sc_recommend_feed limit="6"]');?>
  </div>
</div>

<div class="box-social sp-none">
  <div class="box-twitter">
    <div id="tw-page">
      <a class="twitter-timeline" href="https://twitter.com/infact1" data-widget-id="555251250942332929">@infact1 からのツイート</a> 
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script> 
    </div>
  </div>

  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <div class="box-facebook">
    <div class="fb-page" data-href="https://www.facebook.com/infact1/" data-tabs="timeline" data-width="600" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/infact1/"><a href="https://www.facebook.com/infact1/">中小企業売上UP研究所by株式会社インファクト</a></blockquote></div></div>
  </div>
</div>

<div class="box-social pc-none">
  <div class="box-twitter">
  <a href="https://twitter.com/INFACT_JP" target="_blank">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt="中小企業売上UP研究所by株式会社インファクト公式Twitterアカウント">
  </a>
  </div>

  <div class="box-facebook">
  <a href="https://www.facebook.com/infact1/" target="_blank">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" alt="中小企業売上UP研究所by株式会社インファクト公式Facebookページ">
  </a>
  </div>
</div>

<div class="box-contact">
  <a class="sp-none" href="https://www.infact1.co.jp/contact/" target="_blank">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact_img.jpg" alt="お問い合わせ">
  </a>
  <img class="pc_none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact_01.png" alt="お問い合わせ">
  <a class="pc_none tel" href="tel:0356857311">
    <img class="pc_none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact_02.png" alt="お問い合わせ">
  </a>
  <a class="pc_none mail" href="https://www.infact1.co.jp/contact/" target="_blank">
    <img class="pc_none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact_03.png" alt="お問い合わせ">
  </a>
</div>

<!-- END div.post -->
<?php get_footer(); ?>