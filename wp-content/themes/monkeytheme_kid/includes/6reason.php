<?php $pages = get_children(array(
    'post_parent' => 207,
    'post_type'   => 'page',
    'numberposts' => 6,
    'post_status' => 'publish'
));
if(!empty($pages)) : ?>
    <div class="cart-totalsupport archi">
        <div class="col-w">
            <div class="list-item clearfix">
                <?php $i = 0; global $post;
                foreach($pages as $post) : setup_postdata($post); ?>
                    <?php $i++; ?>
                    <div class="col-item">
                        <div class="item match-height">
                            <a href="<?php the_permalink(); ?>">
                                <div class="block-icon">
                                    <i class="img-icon icon-0<?php echo $i; ?>"></i>
                                </div>
                                <h3 class="h3-title"><?php the_title(); ?></h3>
                                <span><?php echo strip_tags(get_the_excerpt()); ?></span>
                                <div class="rect"></div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>