<div class="col-item">
    <div class="item">
        <div class="image">
            <a href="<?php the_permalink(); ?>">
                <?php if(has_post_thumbnail()) the_post_thumbnail(array(247, 186, 'bfi_thumb' => true));
                else { ?>
                    <img src="<?php echo catch_that_image($post->post_content); ?>" width="100%"/>
                <?php } ?>
            </a>
        </div>
        <div class="info-content">
            <h2 class="product-name">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h2>
            <div class="short-description">
                <?php the_excerpt(); ?>
            </div>
            <div class="price-box">
                <span>本体価格</span>
                <span class="price-number"><?php the_field('price'); ?></span>
                <span class="yen">万円</span>
            </div>
        </div>
        <div class="new">
            <span>New</span>
        </div>
    </div>
</div>