<div class="inquiries">
    <div class="n-wrapper">
        <div class="content">
            <h2>保証についてのお問い合わせ</h2>
            <a class="trans white" href="<?php echo get_page_link(326); ?>">よくある質問はこちら</a>
            <a class="trans red" href="<?php echo get_page_link(328); ?>">お問い合わせはこちら</a>
        </div>
    </div>
</div>