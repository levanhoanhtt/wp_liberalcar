<?php $theme_directory = get_stylesheet_directory_uri();?>
<div class="box-line">
    <a class="line line01" href="<?php echo getMyOption('link_line_1', 'javascript:void(0)'); ?>">
        <img class="md" src="<?php echo $theme_directory; ?>/assets/img/home/line-01.jpg" alt=" " />
        <img class="sm" src="<?php echo $theme_directory; ?>/assets/img/home/line-01-sm.png" alt=" " />
    </a>
    <a class="line line02" href="<?php echo getMyOption('link_line_2', 'javascript:void(0)'); ?>">
        <img class="md" src="<?php echo $theme_directory; ?>/assets/img/home/line-02.jpg" alt=" " />
        <img class="sm" src="<?php echo $theme_directory; ?>/assets/img/home/line-02-sm.png" alt=" " />
    </a>
</div>