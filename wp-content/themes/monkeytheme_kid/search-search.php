<?php get_header(); ?>



<div class="post archive-posts cat-posts  kizi"> 
<?php
$s = $_GET['s'];
$type = $_GET['maker'];
$type02 = $_GET['bodytype'];
$low = $_GET['low'];
$high = $_GET['high'];
$low02 = $_GET['low02'];
$high02 = $_GET['high02'];
$type03 = $_GET['inspection'];
$type04 = $_GET['repair'];
$type05 = $_GET['shift'];
$type06 = $_GET['opnavi'];
$type07 = $_GET['opsun'];
$type08 = $_GET['opseat'];
 
//meta_query用
if($type){
    $metaquerysp[] = array(
            'key'=>'maker',
            'value'=> $type,
            );
}
if($type02){
    $metaquerysp[] = array(
            'key'=>'bodytype',
            'value'=> $type02,
            );
}
$metaquerysp['relation'] = 'AND';
$metaquerysp[] = array(
            'key'=>'price',
            'value'=>array( $low, $high ),
            'compare'=>'BETWEEN',
            'type'=>'NUMERIC',
            );
$metaquerysp[] = array(
            'key'=>'distance',
            'value'=>array( $low02, $high02 ),
            'compare'=>'BETWEEN',
            'type'=>'NUMERIC',
            );
if($type03){
    $metaquerysp[] = array(
            'key'=>'inspection',
            'value'=> $type03,
            );
}
if($type04){
    $metaquerysp[] = array(
            'key'=>'repair',
            'value'=> $type04,
            );
}
if($type05){
    $metaquerysp[] = array(
            'key'=>'shift',
            'value'=> $type05,
            );
}
if($type06){
    $metaquerysp[] = array(
            'key'=>'opnavi',
            'value'=> $type06,
            );
}
if($type07){
    $metaquerysp[] = array(
            'key'=>'opsun',
            'value'=> $type07,
            );
}
if($type08){
    $metaquerysp[] = array(
            'key'=>'opseat',
            'value'=> $type08,
            );
}
?>


 <?php get_template_part('custum_form');?>
<div>検索条件</div>
<?php if($s){ ?>検索キーワード：<?php echo $s; ?><br><?php } ?>
<?php if($type){ ?>メーカー：<?php echo $type; } ?><br>
<?php if($type02){ ?>ボディタイプ：<?php echo $type02; } ?><br>
<?php if($low == 0 && $high == 9999999){}else{ ?>価格帯：<?php if($low == 0){ ?>下限なし<?php }else{ echo number_format($low).'万円'; } ?> ～ <?php if($high == 999999){ ?>上限なし<?php }else{ echo number_format($high).'万円';} ?><?php } ?><br>
<?php if($low02 == 0 && $high02 == 9999999){}else{ ?>走行距離：<?php if($low02 == 0){ ?>下限なし<?php }else{ echo number_format($low02).'km'; } ?> ～ <?php if($high02 == 999999){ ?>上限なし<?php }else{ echo number_format($high02).'km';} ?><?php } ?><br>
<?php if($type03){ ?>車検：<?php echo $type03; } ?><br>
<?php if($type04){ ?>修復歴：<?php echo $type04; } ?><br>
<?php if($type05){ ?>シフト：<?php echo $type05; } ?><br>
<?php if($type06 || $type07 || $type08){ ?>オプション：<?php } ?><?php if($type06){ ?><?php echo $type06; } ?> <?php if($type07){ ?><?php echo $type07; } ?> <?php if($type08){ ?><?php echo $type08; } ?><br>
 
<div>検索結果だよ</div>
 
<?php
query_posts( array(
    'post_type' => 'search',
    'meta_query' => $metaquerysp,
    's' => $s,
    )
);
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
<div><?php the_title(); ?></div>
 
<?php endwhile; else : ?>
 
<div>該当なし</div>
 
<?php endif;
wp_reset_query(); ?>
<h2>ここまで</h2>

  <div class="post-wrap clearfix">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article class="entry">
        <div class="entry-wrap clearfix">

          <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

            <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>

            <?php

            $title= get_the_title();

            the_post_thumbnail(array( 285,190 ),

            array( 'alt' =>$title, 'title' => $title)); ?>

            <?php else: // サムネイルを持っていないときの処理 ?>

            <img src="<?php echo catch_that_image($post->post_content); ?>" width="285" height="190" />

            <?php endif; ?>

            </a> </div>

          <!-- /.sumbox -->

          <div class="entry-content">

            <h3 class="entry-title"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

              <?php the_title(); ?>

              </a></h3>

          </div>

          <!-- .entry-content -->

        </div>

      </article>

      <!--/entry-->

  <?php endwhile; else: ?>

  <p>申し訳ありません。探している記事は現在ありません。</p>

  <?php endif; ?>

  </div>

  <div style="padding:20px 0px;">

    <?php get_template_part('ad');?>

  </div>

  <!--ページナビ-->

  <?php if (function_exists("pagination")) {

    pagination($wp_query->max_num_pages);

} ?>

  <!--ループ終了--> 

</div>

<!-- END div.post -->

<?php get_footer(); ?>

