<?php get_header(); ?>


<div class="archive-posts cat-posts"> 

  <!--ループ開始-->
  <div class="cat-description"><?php echo category_description(); ?></div>
  <h2 class="cat-title">「
    <?php if( is_category() ) { ?>

    <?php single_cat_title(); ?>

    <?php } elseif( is_tag() ) { ?>

    <?php single_tag_title(); ?>

    <?php } elseif( is_tax() ) { ?>

    <?php single_term_title(); ?>

    <?php } elseif (is_day()) { ?>

    日別アーカイブ：<?php echo get_the_time('Y年m月d日'); ?>

    <?php } elseif (is_month()) { ?>

    月別アーカイブ：<?php echo get_the_time('Y年m月'); ?>

    <?php } elseif (is_year()) { ?>

    年別アーカイブ：<?php echo get_the_time('Y年'); ?>

    <?php } elseif (is_author()) { ?>

    投稿者アーカイブ：<?php echo esc_html(get_queried_object()->display_name); ?></h2>

  <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>

    ブログアーカイブ

    <?php } ?>

  」 一覧

  </h2>

  <!-- /#dendo -->
  <div class="post-wrap clearfix">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article class="entry">
      <div class="entry-wrap clearfix">

        <div class="sumbox"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

          <?php if ( has_post_thumbnail() ): // サムネイルを持っているときの処理 ?>

          <?php

          $title= get_the_title();

          the_post_thumbnail(array( 285,190 ),

          array( 'alt' =>$title, 'title' => $title)); ?>

          <?php else: // サムネイルを持っていないときの処理 ?>

          <img src="<?php echo catch_that_image($post->post_content); ?>" width="285" height="190" />

          <?php endif; ?>

          </a> </div>

        <!-- /.sumbox -->

        <div class="entry-content">

          <h3 class="entry-title"> <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

            <?php the_title(); ?>

            </a></h3>

        </div>

        <!-- .entry-content -->

      </div>

    </article>

    <!--/entry-->


    <?php endwhile; else: ?>

    <p>記事がありません</p>

    <?php endif; wp_reset_query(); ?>
  </div><!--post-wrap-->
  <!--ページナビ-->
  <?php if (function_exists("pagination")) {
pagination($wp_query->max_num_pages);
} ?>
  <!--ループ終了--> 

</div>

<!-- END div.post -->

<?php get_footer(); ?>
