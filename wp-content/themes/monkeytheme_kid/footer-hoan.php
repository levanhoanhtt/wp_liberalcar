<?php $theme_directory = get_stylesheet_directory_uri();?>
</div>
<footer id="footer">
    <div class="footer-content">
        <div class="footer-about ">
            <div class="clearfix">
                <div class="col-w">
                    <div class="logo-footer">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo getMyOption('footer_logo', $theme_directory.'/assets/img/home/logo-footer.png'); ?>" alt="logo">
                        </a>
                    </div>
                    <div class="info-text"><?php echo getMyOption('footer_text'); ?></div>
                </div>
            </div>
        </div>
        <div class="footer-nav">
            <div class="col-w">
                <div class="clearfix">
                    <div class="list-item clearfix">
                        <div class="col-item col01">
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">リベラルカーズが選ばれる理由</a>
                                    <ul class="link-sub">
                                        <?php global $post;
                                        $pages = get_children(array(
                                            'post_parent' => 207,
                                            'post_type'   => 'page',
                                            'numberposts' => 6,
                                            'post_status' => 'publish'
                                        ));
                                        foreach($pages as $post) : setup_postdata($post); ?>
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-item col02">
                            <ul>
                                <li><a href="<?php echo get_page_link(429); ?>">店舗案内</a></li>
                                <li>
                                    <ul class="link-sub">
                                        <?php $news = new WP_Query(array(
                                            'posts_per_page' => 6,
                                            'post_type' => array('shop')
                                        ));
                                        while($news->have_posts()) : $news->the_post(); ?>
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                        <?php endwhile;wp_reset_postdata(); ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-item col03">
                            <ul>
                                <li><a href="<?php echo get_page_link(532); ?>">会社案内</a></li>
                                <li>
                                    <?php wp_nav_menu(array('theme_location' => 'footer_nav_2', 'container' => '', 'menu_class' => 'link-sub')); ?>
                                </li>
                            </ul>
                        </div>
                        <div class="col-item col04">
                            <?php wp_nav_menu(array('theme_location' => 'footer_nav_1', 'container' => '')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p class="copy"><?php echo getMyOption('footer_copyright'); ?></p>
    </div>
</footer>
<!-- /#container -->
<div class="col-w cover-go-top">
    <div class="backtotop center">
        <img class="sp-none" src="<?php echo $theme_directory; ?>/assets/img/home/backtotop_pc.png" alt="backtotop">
    </div>
</div>
<!--<script type="text/javascript" src="<?php //echo $theme_directory; ?>/assets/js/jquery.min.js"></script>-->
<script type="text/javascript">window.jQuery = window.$ = jQuery;</script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/jquery.matchHeight.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/nav_fix.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/script.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchForm').on('click', 'input.btn-seach-1', function(){
            window.location.href = $('#searchForm').attr('action');
        });
    });
</script>
<?php wp_footer(); ?>
</body>
</html>