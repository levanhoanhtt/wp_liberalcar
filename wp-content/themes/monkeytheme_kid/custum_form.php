<form role="search" method="get" action="<?php bloginfo('search'); ?>">
    <input type="text" name="s" id="s" value="" placeholder="検索">
    <input type="hidden" value="search" name="post_type" id="post_type">

    <p class="clearfix"></p>

    <h3>メーカー</h3>
    <select name="maker">
        <option value="" selected>選択してください</option>
        <option value="トヨタ">トヨタ</option>
        <option value="日産">日産</option>
    </select>

    <h3>ボディタイプ</h3>
    <select name="bodytype">
        <option value="" selected>選択してください</option>
        <option value="コンパクト">コンパクト</option>
        <option value="セダン">セダン</option>
        <option value="ステーションW">ステーションW</option>
    </select>

    <h3>価格帯</h3>
    <select name="low">
        <option value="0" selected>0万円</option>
        <option value="20">20万円</option>
        <option value="40">40万円</option>
        <option value="60">60万円</option>
        <option value="80">80万円</option>
        <option value="100">100万円</option>
        <option value="120">120万円</option>
        <option value="150">150万円</option>
        <option value="200">200万円</option>
        <option value="250">250万円</option>
        <option value="300">300万円</option>
        <option value="400">400万円</option>
    </select>　～
    <select name="high">
        <option value="999999" selected>上限なし</option>
        <option value="20">20万円</option>
        <option value="40">40万円</option>
        <option value="60">60万円</option>
        <option value="80">80万円</option>
        <option value="100">100万円</option>
        <option value="120">120万円</option>
        <option value="150">150万円</option>
        <option value="200">200万円</option>
        <option value="250">250万円</option>
        <option value="300">300万円</option>
        <option value="400">400万円</option>
    </select>

    <h3>走行距離</h3>
    <select name="low02">
        <option value="0" selected>0km</option>
        <option value="10000">10,000km</option>
        <option value="20000">20,000km</option>
        <option value="40000">40,000km</option>
        <option value="60000">60,000km</option>
        <option value="80000">80,000km</option>
        <option value="100000">100,000km</option>
    </select>　～
    <select name="high02">
        <option value="999999" selected>上限なし</option>
        <option value="20000">20,000km</option>
        <option value="40000">40,000km</option>
        <option value="60000">60,000km</option>
        <option value="80000">80,000km</option>
        <option value="100000">100,000km</option>
        <option value="200000">200,000km</option>
    </select>

    <h3>車検</h3>
    <select name="inspection">
        <option value="" selected>こだわらない</option>
        <option value="あり">あり</option>
        <option value="なし">なし</option>
    </select>

    <h3>修復歴</h3>
    <select name="repair">
        <option value="" selected>こだわらない</option>
        <option value="あり">あり</option>
        <option value="なし">なし</option>
    </select>

    <h3>シフト</h3>
    <select name="shift">
        <option value="" selected>こだわらない</option>
        <option value="AT">AT</option>
        <option value="MT">MT</option>
    </select>

    <h3>オプション</h3>
    <label><input type="checkbox" name="opnavi" value="ナビ">ナビ</label><br>
    <label><input type="checkbox" name="opsun" value="サンルーフ">サンルーフ</label><br>
    <label><input type="checkbox" name="opseat" value="本革シート">本革シート</label>

    
    <input type="submit" value="検索">
</form>