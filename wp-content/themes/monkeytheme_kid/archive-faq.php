<?php /* Template Name: FAQ */ ?>
<?php get_header('hoan'); ?>
<?php $headerImage = get_field('header_image', 326);
$subTitle = get_field('header_sub_title', 326);
if(empty($subTitle)) $subTitle = get_the_excerpt(326); ?>
<div class="keyvisual banner-key02"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
    <div class="n-wrapper">
        <?php custom_breadblog(); ?>
        <div class="text-inner">
            <span><?php echo $subTitle; ?></span>
            <h2 class="ttl-key"><?php the_title(); ?></h2>
        </div>
    </div>
</div>
<?php get_template_part('includes/box', 'line'); ?>
<?php $terms = get_terms('faq_model', array('hide_empty' => false));
if(!($terms instanceof WP_Error)){ ?>
    <div class="link-inner faq">
        <div class="n-wrapper">
            <ul>
                <?php foreach($terms as $term){
                    if($term->count > 0){ ?>
                        <li><a class="scroll" href="#faq<?php echo $term->term_id; ?>">Q.<?php echo $term->name; ?></a></li>
                    <?php }
                } ?>
            </ul>
        </div>
    </div>
    <?php foreach($terms as $term) {
        if($term->count > 0){ ?>
            <div class="list-faq" id="faq<?php echo $term->term_id; ?>">
                <div class="n-wrapper">
                    <h2><?php echo $term->name; ?></h2>
                    <div class="content">
                        <?php $faqs = new WP_Query(array(
                            'posts_per_page' => -1,
                            'post_type' => array('faq'),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'faq_model',
                                    'field' => 'id',
                                    'terms' => $term->term_id
                                )
                            )
                        ));
                        while($faqs->have_posts()) : $faqs->the_post(); ?>
                            <div class="box-faq">
                                <div class="heading-accor">
                                    <div class="left">Q</div>
                                    <div class="text"><?php the_title(); ?></div>
                                </div>
                                <div class="body-accor">
                                    <div class="n-cover">
                                        <div class="left">A</div>
                                        <div class="text"><?php the_content(); ?></div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        <?php }
    } ?>
<?php } ?>

<?php get_footer('hoan'); ?>