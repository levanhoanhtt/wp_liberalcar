<?php
// require lib to parse RSS
require_once(__DIR__ . '/lib/Feed.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html <?php language_attributes(); ?> xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<?php if(is_category()): ?>
<?php elseif(is_archive()): ?>
<meta name="robots" content="noindex">
<?php elseif(is_tag()): ?>
<meta name="robots" content="noindex">
<?php endif; ?>
<title>
<?php
global $page, $paged;
if(is_front_page()):
bloginfo('name');
elseif(is_single()):
wp_title('');
elseif(is_page()):
wp_title('');
elseif(is_archive()):
wp_title('|',true,'right');
bloginfo('name');
elseif(is_search()):
wp_title('-',true,'right');
elseif(is_404()):
echo'404 - ';
bloginfo('name');
endif;
if($paged >= 2 || $page >= 2):
echo'-'.sprintf('%sページ',
max($paged,$page));
endif;
?>
</title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
<!---Google+URL--->
<link rel="author" href="https://plus.google.com/+大見知靖19751113/" />
<!-- ここからOGP -->
<meta property="fb:app_id" content="1547795282111685">
<meta property="og:type" content="<?php if($_SERVER["REQUEST_URI"] == "/"){ ?>blog<?php } else { ?>article<?php }?>">
<?php
if (is_single()){//単一記事ページの場合
     if(have_posts()): while(have_posts()): the_post();
          echo '<meta property="og:description" content="'.get_post_meta($post->ID, _aioseop_description, true).'">';echo "\n";//抜粋を表示
     endwhile; endif;
     echo '<meta property="og:title" content="'; the_title(); echo '">';echo "\n";//単一記事タイトルを表示
     echo '<meta property="og:url" content="'; the_permalink(); echo '">';echo "\n";//単一記事URLを表示
} else {//単一記事ページページ以外の場合（アーカイブページやホームなど）
     echo '<meta property="og:description" content="'; bloginfo('description'); echo '">';echo "\n";//「一般設定」管理画面で指定したブログの説明文を表示
     echo '<meta property="og:title" content="'; bloginfo('title'); echo '">';echo "\n";//「一般設定」管理画面で指定したブログのタイトルを表示
     echo '<meta property="og:url" content="'; echo home_url('/'); echo '">';echo "\n";//「一般設定」管理画面で指定したブログのURLを表示
}
?>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
<?php
$str = $post->post_content;
$searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';//投稿にイメージがあるか調べる
if (is_single()){//単一記事ページの場合
if (has_post_thumbnail()){//投稿にサムネイルがある場合の処理
     $image_id = get_post_thumbnail_id();
     $image = wp_get_attachment_image_src( $image_id, 'full');
     echo '<meta property="og:image" content="'.$image[0].'">';echo "\n";
} else if ( preg_match( $searchPattern, $str, $imgurl ) && !is_archive()) {//投稿にサムネイルは無いが画像がある場合の処理
     echo '<meta property="og:image" content="'.$imgurl[2].'">';echo "\n";
} else {//投稿にサムネイルも画像も無い場合の処理
     echo '<meta property="og:image" content="'; echo home_url('/ogp.jpg'); echo '">';echo "\n";
}
} else {//単一記事ページページ以外の場合（アーカイブページやホームなど）
     echo '<meta property="og:image" content="'; echo home_url('/ogp.jpg'); echo'">';echo "\n";
}
?>
<!--Twitterカード-->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@INFACT_JP" />
<?php if(is_home()): ?>  <!-- ←でブログのトップページを判定 -->
<meta name="twitter:title" content="<?php bloginfo('name'); ?>">
<meta name="twitter:description" content="<?php bloginfo('description'); ?>">
<meta name="twitter:image:src" content="http://www.infact1.co.jp/ogp.jpg">
<?php elseif(is_page()): ?> <!-- ←で固定ページを判定 -->
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:description" content="<?php bloginfo('description'); ?>">
<meta name="twitter:image:src" content="http://www.infact1.co.jp/ogp.jpg">
<?php else: ?> <!-- ←上記の条件にもれたページ（記事ページ） -->
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:description" content="<?php echo mb_substr(str_replace(array("rn", "r", "n"), '', strip_tags($post-> post_content)), 0, 100).'...'; ?>">
<?php endif; ?>
<?php $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large'); ?>    
<?php if(is_single() && has_post_thumbnail() ): ?>
<meta name="twitter:image:src" content="<?php echo $image_url[0] ?>">
<?php elseif(!is_home() && !is_page() ): ?>
<meta name="twitter:image:src" content="http://www.infact1.co.jp/ogp.jpg">
<?php endif; ?>
<!-- ここまでOGP -->
<!---css切り替え--->
<?php if(is_mobile()) { ?>
<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon-precomposed.png" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/smart.css" type="text/css" media="all" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<?php } else { ?>
<meta name="viewport" content="width=1024, maximum-scale=1, user-scalable=yes">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
<?php } ?>
<?php wp_head(); ?>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js?ver=4.3.1'></script>
<!---エバーノート--->
<script type="text/javascript" src="http://static.evernote.com/noteit.js"></script>
<!-- アナリティクス -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-47774552-1', 'infact1.co.jp');
  ga('send', 'pageview');
</script>

<!-- webマスター -->
<meta name="google-site-verification" content="eOECF3RdLqPSnPhNQ9MCB3M4UCNw9F5b2mH2ThlUqrU" />
<!-- ナビゲーションfix -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/nav_fix.js"></script>
<?php if(is_mobile()) { ?>
<!-- スライドメニュー -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/sp_slidemenu.js"></script>
<?php } ?>
<!-- other scripts -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/script.js"></script>
</head>


<body <?php body_class(); ?>>
<!--FB-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=1547795282111685&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--endFB-->
<?php if(is_mobile()) { ?>
<?php
}else{
?>
<?php
}
?>

<div id="container">
  <!-- /#header -->
  <div id="gazou">
    <div id="gazou-in">
      <?php if (is_home()) { ?>
     
      <?php } else { ?>
      <?php //カスタムヘッダー画像// ?>
      <?php if(get_header_image()): ?>
      <p id="headimg"><a href="<?php echo home_url();?>"><img src="<?php header_image(); ?>" alt="*" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" /></a></p>
      <?php endif; ?>
      <?php } ?>
    </div>
    <!-- /#gazou-in --> 
  </div>
  <!-- /#gazou -->
<?php if(is_mobile()) { ?>
<?php 
}else{
?>

<?php
}
?>
  <!--pcnavi-->
  <div class="navig-wrap">
    <div id="navi-in" class="clearfix">
      <div class="logo">
        <h1 class="sitename">
          <a href="<?php echo home_url(); ?>/">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title.png" alt="logo">
          </a>
        </h1>
      </div>
      <div class="header-right sp-none">
        <span class="phone"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_phone.png" alt="phone">03-5685-7311</span>
        <a class="email" href="/contact"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_email.png" alt="email"></a>
      </div>
      <div class="navbar sp-none">
        <?php wp_nav_menu(array('theme_location' => 'navbar'));?>
      </div>
    </div>
  </div>
  <!--/pcnavi-->
  <div class="page-feature-image">
    <img class="sp-none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/service_banner_top.png" alt="featured">
    <img class="pc_none" src="<?php echo get_stylesheet_directory_uri(); ?>/images/service_banner_top_sp.png" alt="featured">
  </div>



<div id="wrap">

<div id="wrap-in">
  <div id="breadcrumb"><?php custom_breadcrumbs(); ?>  </div><!--/breadcrumb-->
<div id="top_con">
<div id="main">