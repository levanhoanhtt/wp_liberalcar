<?php get_header('hoan'); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/blog.css" type="text/css" media="all"/>
<?php $fieldObjs = get_field_objects(38);
$fields = array();
if ($fieldObjs && !empty($fieldObjs)) {
    foreach ($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
}
$headerImage = getObjectValue($fields, 'header_image'); ?>
<div class="keyvisual archi"<?php if (!empty($headerImage)) echo ' style="background: url(' . $headerImage . ') no-repeat center center;"'; ?>>
    <div class="n-wrapper">
        <?php custom_breadblog(); ?>
        <div class="text-inner">
            <span><?php echo getObjectValue($fields, 'header_sub_title'); ?></span>
            <h2 class="ttl-key"><?php echo get_the_title(38); ?></h2>
        </div>
    </div>
</div>
<?php get_template_part('includes/box', 'line'); ?>
    <div class="new-value">
        <div class="n-wrapper">
            <div class="archive-posts cat-posts">
                <ul class="category_list">
                    <?php $cat_all = get_terms("category", "fields=all&get=all");
                    foreach ($cat_all as $value):?>
                        <li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <div id="dendo"></div>
                <div class="post-wrap clearfix">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article class="entry">
                            <div class="entry-wrap clearfix">
                                <div class="sumbox">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                                        <?php if (has_post_thumbnail()): // サムネイルを持っているときの処理 ?>
                                            <?php $title = get_the_title();
                                            the_post_thumbnail(array(285, 190), array('alt' => $title, 'title' => $title)); ?>
                                        <?php else: // サムネイルを持っていないときの処理 ?>
                                            <img src="<?php echo catch_that_image($post->post_content); ?>" width="285" height="190"/>
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="entry-content">
                                    <h3 class="entry-title">
                                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; else: ?>
                        <p>記事がありません</p>
                    <?php endif; wp_reset_query(); ?>
                </div>
                <?php if (function_exists("pagination")) pagination($wp_query->max_num_pages);?>
            </div>
        </div>
    </div>
<?php get_footer('hoan'); ?>