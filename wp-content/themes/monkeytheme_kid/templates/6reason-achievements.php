<?php /* Template Name: Achievements - ShowCase */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual archi"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php if(isset($fields['achievement']) && !empty($fields['achievement'])){ ?>
        <div class="new-value">
            <div class="n-wrapper">
                <h2 class="ttl-value"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
                <?php $i = 0;
                foreach($fields['achievement'] as $a){
                    $i++; ?>
                    <div class="box-value clearfix<?php if($i % 2 == 0) echo ' revert';?>">
                        <div class="text">
                            <div class="ttl-sub"><?php echo $a['title']; ?></div>
                            <?php echo $a['description']; ?>
                        </div>
                        <div class="image">
                            <img src="<?php echo $a['image']; ?>" alt="<?php echo $a['title']; ?>" />
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if(isset($fields['history']) && !empty($fields['history'])){ ?>
        <div class="history">
            <div class="n-wrapper">
                <div class="ttl-history">沿革</div>
                <ul>
                    <?php foreach($fields['history'] as $h){ ?>
                        <li><span class="year"><?php echo $h['year'] ?></span><span class="text"><?php echo $h['event']; ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } ?>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>