<?php /* Template Name: Hight Purchase */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual purchase"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php if(isset($fields['reasons']) && !empty($fields['reasons'])){ ?>
    <div class="new-value">
        <div class="n-wrapper">
            <h2 class="ttl-value"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
            <?php $i = 0;
            foreach($fields['reasons'] as $r){
                $i++; ?>
                <div class="box-value clearfix<?php if($i % 2 == 0) echo ' revert'; ?>">
                    <div class="text">
                        <div class="ttl-sub"><span>理由その<?php echo $i; ?></span><br><?php echo $r['title']; ?></div>
                        <?php echo $r['description']; ?>
                    </div>
                    <div class="image">
                        <img src="<?php echo $r['image']; ?>" alt="<?php echo $r['title']; ?>" />
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    <div class="inquiries purcha">
        <div class="n-wrapper">
            <div class="content">
                <h2>お車の乗り換え・買い換えで旧車を手放す時、<br><span>リベラルカーズ</span>の名前を思い出してください！</h2>
                <a class="trans red" href="#">買取査定のお申し込みはこちら</a>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>