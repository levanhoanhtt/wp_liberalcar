<?php /* Template Name: Home page */ ?>
<?php get_header('hoan'); ?>
<?php $theme_directory = get_stylesheet_directory_uri();?>
    <div class="rev-wrap">
        <?php echo do_shortcode('[rev_slider alias="homepage"]'); ?>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <!-- Box News -->
    <?php global $post; $categoryId = 9;
    $news = get_posts(array('posts_per_page'=> 3,'category'=> $categoryId));
    if(!empty($news)) : ?>
        <div class="box-news">
            <div class="col-w">
                <div class="block-title">
                    <h3>更新情報</h3>
                    <span>WHAT'S NEW</span>
                </div>
                <table class="list-news">
                    <?php foreach($news as $post) : setup_postdata($post); ?>
                        <tr>
                            <td>
                                <div class="date"><?php the_time('Y.m.d'); ?></div>
                            </td>
                            <td>
                                <div class="text-new">ニュース</div>
                            </td>
                            <td>
                                <div class="info-new">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; wp_reset_postdata(); ?>
                </table>
                <div class="link-more">
                    <a class="active" href="<?php echo get_category_link($categoryId); ?>">詳しく見る <i class="icon-arrow"></i></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- End Box News -->
    <!-- Cart Totalsupport -->
    <?php $page = get_post(207);
    if($page) : ?>
    <div class="cart-totalsupport">
        <div class="col-w">
            <div class="title">
                <h2><?php echo $page->post_title; ?></h2>
                <span>liberal cars totalsupport</span>
            </div>
            <div class="info-head">
                <p><?php echo $page->post_excerpt; ?></p>
            </div>
            <div class="list-item clearfix">
                <?php $pages = get_children(array(
                    'post_parent' => $page->ID,
                    'post_type'   => 'page',
                    'numberposts' => 6,
                    'post_status' => 'publish'
                ));
                $i = 0;
                foreach($pages as $post) : setup_postdata($post); ?>
                    <?php $i++; ?>
                    <div class="col-item">
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="block-icon">
                                    <i class="img-icon icon-0<?php echo $i; ?>"></i>
                                </div>
                                <h3 class="h3-title full match-height"><?php the_title(); ?></h3>
                                <span><?php the_excerpt(); ?></span>
                                <div class="image">
                                    <?php if(has_post_thumbnail()) the_post_thumbnail(array(324, 155, 'bfi_thumb' => true));
                                    else { ?>
                                        <img src="<?php echo catch_that_image($post->post_content); ?>" width="100%"/>
                                    <?php } ?>
                                </div>
                                <div class="rect"></div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- End Cart Totalsupport -->
	
    <!-- Useful Info -->
	<?php $news = get_posts(array('posts_per_page'=> 4));
	if(!empty($news)): ?>
        <div class="useful-info">
            <div class="col-w">
                <div class="block-title">
                    <h3>お役立ち情報</h3>
                    <span>NEW TOPICS</span>
                </div>
                <div class="list-item clearfix">
                <?php foreach($news as $post) : setup_postdata($post); ?>
                    <div class="col-item">
                        <div class="item">
                            <div class="image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if(has_post_thumbnail()) the_post_thumbnail(array(247, 165, 'bfi_thumb' => true));
                                    else { ?>
                                        <img src="<?php echo catch_that_image($post->post_content); ?>" width="100%"/>
                                    <?php } ?>
                                    <!--<div class="caption-text">ダミー</div>-->
                                </a>
                            </div>
                            <h3 class="h3-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h3>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
                </div>
                <div class="link-more">
                    <a href="<?php echo get_page_link(38); ?>">詳しく見る <i class="icon-arrow"></i></a>
                </div>
            </div>
        </div>
	<?php endif; ?>
    <!-- End Useful Info -->

    <!-- Recommended -->
    <?php $news = new WP_Query(array(
        'posts_per_page' => 8,
        'post_type' => array('search')
    ));
    if($news->have_posts()) : ?>
        <div class="recommended">
            <div class="col-w">
                <div class="block-title">
                    <h3>オススメ車両情報</h3>
                    <span>Recommended</span>
                </div>
                <div class="list-item clearfix">
                    <?php while($news->have_posts()) : $news->the_post(); ?>
                        <?php get_template_part('includes/loop', 'product'); ?>
                    <?php endwhile;wp_reset_postdata(); ?>
                </div>
                <div class="link-more">
                    <a href="<?php echo get_post_type_archive_link('search'); ?>">車両情報一覧を見る <i class="icon-arrow"></i></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- End Recommended -->
<?php get_footer('hoan'); ?>