<?php /* Template Name: Recruit info */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual recruit"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <!--<h2 class="ttl-key"><?php //the_title(); ?></h2>-->
                <h2 class="ttl-key">募集要項・採用エントリー<br/>新卒採用・中途採用</h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/vuong-fix.css" type="text/css" media="all" />
    <div class="n-recruit">
        <div class="n-wrapper">
            <h2 class="ttl-value-cmn">私たちと一緒に働きませんか？</h2>
            <div class="box-value clearfix revert rcu">
                <div class="text"><?php echo getObjectValue($fields, 'text_why'); ?></div>
                <div class="image">
                    <img src="<?php echo getObjectValue($fields, 'image_why'); ?>" alt=" ">
                </div>
            </div>
        </div>
    </div>
    <?php global $post;
    $news = get_posts(array('posts_per_page'=> 4));
        if(!empty($news)){ ?>
        <div class="n-staff">
            <div class="n-wrapper">
                <h2>スタッフの声</h2>
                <div class="cover clearfix">
                    <?php foreach($news as $post) : setup_postdata($post); ?>
                        <div class="inner">
                            <a href="<?php the_permalink(); ?>">
                                <?php if(has_post_thumbnail()) the_post_thumbnail(array(241, 161, 'bfi_thumb' => true));
                                else { ?>
                                    <img src="<?php echo catch_that_image($post->post_content); ?>" width="100%"/>
                                <?php } ?>
                                <p><?php the_title(); ?></p>
                            </a>
                        </div>
                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if(isset($fields['requirements']) && !empty($fields['requirements'])){ ?>
        <div class="requirements">
            <div class="n-wrapper">
                <h2>募集要項</h2>
                <?php foreach($fields['requirements'] as $re){ ?>
                    <div class="box-requirements">
                        <h3><?php echo $re['title']; ?></h3>
                        <?php echo $re['description']; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if(isset($fields['steps']) && !empty($fields['steps'])){ ?>
        <div class="n-flow">
            <div class="n-wrapper">
                <h2>採用の流れ</h2>
                <div class="desc3">正社員採用の場合の一例<span>※職種などにより異なる場合がありま</span></div>
                <?php $i = 0;
                foreach($fields['steps'] as $s){
                    $i++; ?>
                    <div class="step">
                        <div class="ttl-setp"><span>【STEP<?php echo $i; ?>】</span><?php echo $s['title']; ?></div>
                        <?php echo $s['description']; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="n-plusbox">
        <div class="n-wrapper">
            <div class="box-value clearfix rcu">
                <div class="text">
                    <div class="ttl-sub"><?php echo getObjectValue($fields, 'title_2'); ?></div>
                    <?php echo getObjectValue($fields, 'description_2'); ?>
                </div>
                <div class="image">
                    <img src="<?php echo getObjectValue($fields, 'image_2'); ?>" alt=" ">
                </div>
            </div>
        </div>
    </div>
    <div class="entry-form">
        <div class="n-wrapper">
            <h2>エントリーフォーム</h2>
            <div class="desc">以下のフォームに必要事項をご入力の上、送信ボタンをクリックして下さい。</div>
            <?php echo do_shortcode('[trust-form id=570]'); ?>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>