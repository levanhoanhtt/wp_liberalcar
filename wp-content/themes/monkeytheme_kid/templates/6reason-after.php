<?php /* Template Name: After maintain */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual maintaince"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <div class="block-maintaince">
        <div class="n-wrapper">
            <h2 class="ttl-value-cmn"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
            <?php $pages = get_children(array(
                'post_parent' => 218,
                'post_type'   => 'page',
                'numberposts' => -1,
                'post_status' => 'publish'
            ));
            if(!empty($pages)) : ?>
                <div class="clearfix">
                    <?php global $post;
                    foreach($pages as $post) : setup_postdata($post); ?>
                        <div class="box-maintaince">
                            <?php if(has_post_thumbnail()) the_post_thumbnail(array(320, 192, 'bfi_thumb' => true));
                            else { ?>
                                <img src="<?php echo catch_that_image($post->post_content); ?>" width="100%"/>
                            <?php } ?>
                            <h3 class="ttl-main"><?php the_title(); ?></h3>
                            <p class="match-height"><?php echo strip_tags(get_the_excerpt()); ?></p>
                            <a class="trans" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>