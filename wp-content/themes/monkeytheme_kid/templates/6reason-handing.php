<?php /* Template Name: Handling of new car */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual hading"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php $theme_directory = get_stylesheet_directory_uri();?>
    <div class="new-value handing">
        <div class="n-wrapper">
            <h2 class="ttl-value"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
            <?php if(isset($fields['handling']) && !empty($fields['handling'])){
                $i = 0;
                foreach($fields['handling'] as $h){
                    $i++; ?>
                    <div class="box-value clearfix<?php if($i % 2 == 1) echo ' revert'; ?>">
                        <div class="text">
                            <div class="ttl-sub"><?php echo $h['title']; ?></div>
                            <?php echo $h['description']; ?>
                        </div>
                        <div class="image">
                            <img src="<?php echo $h['image']; ?>" alt="<?php echo $h['title']; ?>" />
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
    <?php if(isset($fields['inspections']) && !empty($fields['inspections'])){ ?>
        <div class="n-inspection">
            <div class="n-wrapper">
                <h2 class="ttl-inspection">点検</h2>
                <div class="content">
                    <ul>
                        <?php foreach($fields['inspections'] as $in){ ?>
                            <li><?php echo $in['title']; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="sheet-metal">
        <div class="n-wrapper">
            <h2 class="ttl-sheet-metal">板金</h2>
            <div class="content">
                納車前から1年間!<br>板金修理代を<span>板金修理代を</span>まで保証！
            </div>
        </div>
    </div>
    <div class="maintenance">
        <div class="n-wrapper">
            <h2 class="ttl-maintenance">メンテナンスサポートパック料金表</h2>
            <div class="cover">
                <?php the_content(); ?>
            </div>
            <div class="box-bottom">
                <div class="sub-ttl">ご利用について</div>
                <p><?php echo getObjectValue($fields, 'about_use'); ?></p>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'inquiries'); ?>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>