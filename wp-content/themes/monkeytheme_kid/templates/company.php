<?php /* Template Name: Company outline */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual outline"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <div class="philosophy">
        <div class="n-wrapper">
            <h2 class="ttl-philosophy" id="sec01">企業理念</h2>
            <div class="desc">「お客様、社員、社会に必要とされる企業の創造」</div>
            <?php if(isset($fields['corporate_philosophy']) && !empty($fields['corporate_philosophy'])){
                foreach($fields['corporate_philosophy'] as $cp){ ?>
                    <div class="box-phil">
                        <h3><?php echo $cp['title']; ?></h3>
                        <?php echo $cp['description']; ?>
                    </div>
                <?php }
            } ?>
            <div class="n-message">
                <h3>代表挨拶</h3>
                <div class="clearfix">
                    <div class="avatar">
                        <?php $name = getObjectValue($fields, 'president_name'); ?>
                        <img src="<?php echo getObjectValue($fields, 'president_image'); ?>" alt="<?php echo $name; ?>" />
                        <p><?php echo $name; ?></p>
                        <img src="<?php echo getObjectValue($fields, 'president_name_image'); ?>" alt="<?php echo $name; ?>" />
                    </div>
                    <div class="text">
                        <h4><?php echo getObjectValue($fields, 'message_heder'); ?></h4>
                        <div class="desc2"><?php echo getObjectValue($fields, 'message_sub_heder'); ?></div>
                        <?php echo getObjectValue($fields, 'message_content'); ?>
                    </div>
                </div>
            </div>
            <div class="infor-company" id="sec02">
                <h3>会社概要</h3>
                <?php the_content(); ?>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>