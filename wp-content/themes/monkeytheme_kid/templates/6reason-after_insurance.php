<?php /* Template Name: Cars insurance */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual insurance"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php $theme_directory = get_stylesheet_directory_uri();?>
    <div class="insurance">
        <div class="n-wrapper">
            <h2 class="ttl-value-cmn"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
            <div class="box-insurance">
                <div class="ttl-inner">保険のプロがわかりやすく自動車保険の<br />サポートをいたします！</div>
                <div class="content clearfix">
                    <div class="left"><img src="<?php echo getObjectValue($fields, 'image_different_1'); ?>" alt=" " /></div>
                    <div class="center">こんなに<br />違う!!</div>
                    <div class="right"><img src="<?php echo getObjectValue($fields, 'image_different_2'); ?>" alt=" " /></div>
                </div>
            </div>
            <div class="box-features">
                <h3 class="ttl-features">リベラルカーズの自動車保険の特長!</h3>
                <?php if(isset($fields['insurance_features']) && !empty($fields['insurance_features'])){
                    $i = 0;
                    foreach($fields['insurance_features'] as $if){
                        $i++; ?>
                        <div class="box-inner">
                            <p><span><?php echo $i; ?></span><?php echo $if['title']; ?></p>
                            <?php if(!empty($if['description'])){ ?>
                                <div class="note">※<?php echo $if['description']; ?></div>
                            <?php } ?>
                        </div>
                    <?php }
                } ?>
                <div class="box-in-bottom">「保険内容の見直し」などもお気軽に当社スペシャリストにご相談ください!!</div>
            </div>
            <?php if(isset($fields['insurance_company']) && !empty($fields['insurance_company'])){ ?>
                <div class="box-isurance-company">
                    <h3 class="ttl-box-isurance-company">引受保険会社</h3>
                    <div class="inner">
                        <?php foreach($fields['insurance_company'] as $ic){ ?>
                            <div class="clearfix">
                                <div class="left">
                                    <img src="<?php echo $ic['image']; ?>" alt="<?php echo $ic['title']; ?>" />
                                </div>
                                <div class="right">
                                    <p><?php echo $ic['title']; ?></p>
                                    <a href="<?php echo $ic['link']; ?>"><?php echo $ic['link']; ?></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if(isset($fields['insurance_different']) && !empty($fields['insurance_different'])){ ?>
                <div class="n-different">
                    <h3 class="ttl-different">ここが違う！リベラルカーズの自動車保険</h3>
                    <?php $i = 0;
                    foreach($fields['insurance_different'] as $id){
                        $i++; ?>
                        <div class="box-inner">
                            <div class="sub-ttl"><span><?php echo $i; ?></span><?php echo $id['title']; ?></div>
                            <?php echo $id['description']; ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="n-policy">
                <div class="ttl-n-policy">勧誘方針</div>
                <div class="boc-n-polycy">
                    <?php echo getObjectValue($fields, 'invitation_policy'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'inquiries'); ?>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>