<?php
/*
Template Name: お問い合わせ/ Contact
*/
?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual recruit"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/vuong-fix.css" type="text/css" media="all" />
    <div class="entry-form contact">
        <div class="n-wrapper">
            <h2 class="ttl-value-cmn">どんな質問でもお答えします！</h2>
            <div class="desc">情報をご入力ください</div>
			<?php echo do_shortcode('[trust-form id=550]'); ?>
		</div>
	</div>
		
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>