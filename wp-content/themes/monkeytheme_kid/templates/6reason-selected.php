<?php /* Template Name: Selected */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual banner-key02"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <div class="selected-purchase">
        <div class="col-w">
            <div class="box-info">
                <?php the_content(); ?>
            </div>
            <?php if(isset($fields['steps']) && !empty($fields['steps'])){
                $i = 0; ?>
                <div class="box-step">
                    <?php foreach($fields['steps'] as $s){
                        $i++; ?>
                        <div class="item">
                            <h3><span>STEP<?php echo $i; ?></span> <?php echo $s['step_name'] ?></h3>
                            <?php $j = 0;
                            foreach($s['step_list'] as $t){
                                $j++;?>
                                <div class="w-full clearfix<?php if($j %2 == 0) echo '  revert'; ?>">
                                    <div class="w-left">
                                        <div class="image">
                                            <img src="<?php echo $t['image']; ?>" alt="<?php echo $t['title']; ?>" />
                                        </div>
                                    </div>
                                    <div class="w-right">
                                        <div class="text">
                                            <h4><?php echo $t['title']; ?></h4>
                                            <?php echo $t['description']; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>