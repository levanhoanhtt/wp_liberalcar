<?php /* Template Name: Cars Warranty */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual warranty"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php $theme_directory = get_stylesheet_directory_uri();?>
    <div class="guarantee">
        <div class="n-wrapper">
            <h2 class="ttl-value-cmn"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
            <div class="top-guarantee">
                <h3 class="ttl-guarantee">走行距離無制限・最長2年間保証!!</h3>
                <div class="clearfix">
                    <div class="left">
                        <h4>保証内容</h4>
                        <?php if(isset($fields['steps']) && !empty($fields['steps'])){
                            $i = 0;
                            foreach($fields['steps'] as $s){
                                $i++; ?>
                                <div class="dis-tbl">
                                    <div class="num"><?php echo $i; ?></div>
                                    <div class="text"><p><?php echo $s['title']; ?></p></div>
                                </div>
                            <?php }
                        } ?>
                    </div>
                    <div class="right">
                        <h4><?php echo getObjectValue($fields, 'detail_title'); ?></h4>
                        <?php echo getObjectValue($fields, 'detail_description'); ?>
                    </div>
                </div>
            </div>
            <div class="top-guarantee reveert">
                <div class="clearfix">
                    <div class="left">
                        <img src="<?php echo getObjectValue($fields, 'without_warranty_image'); ?>" alt=" " />
                    </div>
                    <div class="right">
                        <h4>保証に入っていないと・・・</h4>
                        <?php echo getObjectValue($fields, 'without_warranty_description'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($fields['plans']) && !empty($fields['plans'])){ ?>
        <div class="plan">
            <div class="n-wrapper">
                <h2 class="ttl-plan">保証プランについて</h2>
                <h3 class="desc">安心してお車に乗っていただきたい<br>それが私たちの「願い」です</h3>
                <?php foreach($fields['plans'] as $p){ ?>
                    <div class="box-plan">
                        <h4><?php echo $p['title']; ?></h4>
                        <div class="table">
                           <?php echo $p['description']; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="example">
        <div class="n-wrapper">
            <h2 class="ttl-ex">２年保証の例（購入時に車検取得）</h2>
            <div class="content-ex">
                <ul class="clearfix">
                    <li class="bg">納車</li>
                    <li class="arrow"><img src="<?php echo $theme_directory; ?>/assets/img/page03/icon02.png" alt=" " />２年後</li>
                    <li class="bg">車検</li>
                    <li class="arrow"><img src="<?php echo $theme_directory; ?>/assets/img/page03/icon02.png" alt=" " />２年後</li>
                    <li class="bg">車検</li>
                    <li class="arrow"><img src="<?php echo $theme_directory; ?>/assets/img/page03/icon02.png" alt=" " />２年後</li>
                    <li class="last">ずっと無料! <span>回数制限無し</span></li>
                </ul>
                <div class="bottom"><span>２年後の保証満了日</span>後も・・・</div>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'inquiries'); ?>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>