<?php /* Template Name: Shop */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual banner-key03"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <div class="shop-introduction">
        <div class="col-w">
            <h2 class="ttl-value-cmn">お気軽にお越し下さい!!ご来店お待ちしております。</h2>
            <div class="list-item">
                <?php $news = new WP_Query(array(
                    'posts_per_page' => -1,
                    'post_type' => array('shop')
                ));
                while($news->have_posts()) : $news->the_post(); ?>
                    <div class="item">
                        <h3 class="title"><?php the_title(); ?></h3>
                        <div class="w-full clearfix">
                            <div class="w-left">
                                <div class="image">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if(has_post_thumbnail()) the_post_thumbnail(array(320, 217, 'bfi_thumb' => true));
                                        else { ?>
                                            <img src="<?php echo catch_that_image(get_the_content()); ?>" width="100%"/>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                            <div class="w-right">
                                <div class="info-content">
                                    <?php $fieldObjs = get_field_objects();
                                    $fields = array();
                                    if($fieldObjs && !empty($fieldObjs)){
                                        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
                                    }
                                    if(isset($fields['service_list']) && !empty($fields['service_list'])){ ?>
                                        <div class="block-link-shop">
                                            <?php foreach($fields['service_list'] as $s){ ?>
                                                <a href="javascript:void(0)"><?php echo $s['title']; ?></a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <div class="info-text">
                                        <p class="addrest-text"><?php echo getObjectValue($fields, 'street_address'); ?></p>
                                        <p class="addrest-text">最寄駅：<span><?php echo getObjectValue($fields, 'closest_station'); ?></span></p>
                                        <p class="tel-fax">フリーダイヤル：<span><?php echo getObjectValue($fields, 'free_dial'); ?></span>　TEL：<span><?php echo getObjectValue($fields, 'tel'); ?></span>　FAX：<span><?php echo getObjectValue($fields, 'fax'); ?></span></p>
                                        <p class="mail-text">E-Mail：<span><?php echo getObjectValue($fields, 'email'); ?></span></p>
                                    </div>
                                    <div class="link-more-detail">
                                        <a href="<?php the_permalink(); ?>">詳しくはこちら</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile;wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>