<?php /* Template Name: Customer voice */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image');
    $pageLink = get_permalink(); ?>
    <div class="keyvisual voice"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <style>
        .link-inner ul li.active a{color: #f0394d;}
        .n-pager ul li a.active {background: #393939;border-color: #393939;color: #fff;}
    </style>
    <?php $shopTitles = array();
    $shopId = (isset($_GET['shopId']) && $_GET['shopId'] > 0) ? $_GET['shopId'] : 0; ?>
    <div class="link-inner">
        <div class="n-wrapper">
            <h2>店舗を選択</h2>
            <ul>
                <?php $news = new WP_Query(array(
                    'posts_per_page' => -1,
                    'post_type' => array('shop')
                ));
                while($news->have_posts()) : $news->the_post(); ?>
                    <?php $shopId1 = get_the_ID();
                    $shopTitles[$shopId1] = get_the_title(); ?>
                    <li<?php if($shopId == $shopId1) echo ' class="active"'; ?>><a href="<?php echo $pageLink; ?>?shopId=<?php echo $shopId1; ?>"><?php the_title(); ?></a></li>
                <?php endwhile;wp_reset_postdata(); ?>
                <li><a href="<?php echo $pageLink; ?>">ALL</a></li>
            </ul>
        </div>
    </div>
    <div class="list-voice">
        <div class="n-wrapper">
            <h2>お客様の声一覧</h2>
            <?php $paged = (isset($_GET['pg']) && $_GET['pg'] > 0) ? $_GET['pg'] : 1;
            $args = array(
                'posts_per_page' => 10,
                'post_type' => array('customer_voice'),
                'paged' => $paged
            );
            if($shopId > 0){
                $args['meta_key'] = 'shop';
                $args['meta_value'] = $shopId;
            }
            $voices = new WP_Query($args);
            if($voices->have_posts()) : ?>
                <div class="content">
                    <?php while($voices->have_posts()) : $voices->the_post(); ?>
                        <?php if($shopId > 0) $shopId1 = $shopId;
                        else $shopId1 = get_field('shop'); ?>
                        <div class="box-list-voice">
                            <div class="heading"><?php the_title(); ?></div>
                            <div class="bd-voice">
                                <div class="m-height">
                                    <?php the_content(); ?>
                                </div>
                                <div class="note"><?php the_time('Y年m月d日'); ?> <span><?php echo getObjectValue($shopTitles, $shopId1); ?></span></div>
                            </div>
                        </div>
                    <?php endwhile;wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
            <?php if (function_exists("paginationVoice")) paginationVoice($voices->max_num_pages, $paged, $shopId); ?>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>