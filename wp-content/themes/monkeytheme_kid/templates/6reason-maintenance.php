<?php /* Template Name: Reason Maintenance */ ?>
<?php get_header('hoan'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $fieldObjs = get_field_objects();
    $fields = array();
    if($fieldObjs && !empty($fieldObjs)){
        foreach($fieldObjs as $fieldName => $field) $fields[$fieldName] = $field['value'];
    }
    $headerImage = getObjectValue($fields, 'header_image'); ?>
    <div class="keyvisual maintenance"<?php if(!empty($headerImage)) echo ' style="background: url('.$headerImage.') no-repeat center center;"'; ?>>
        <div class="n-wrapper">
            <?php custom_breadblog(); ?>
            <div class="text-inner">
                <span><?php echo getObjectValue($fields, 'header_sub_title', get_the_excerpt()); ?></span>
                <h2 class="ttl-key"><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
    <?php get_template_part('includes/box', 'line'); ?>
    <?php if(isset($fields['maintenances']) && !empty($fields['maintenances'])){ ?>
        <div class="new-value">
            <div class="n-wrapper">
                <h2 class="ttl-value"><?php echo getObjectValue($fields, 'page_title'); ?></h2>
                <?php $i = 0;
                foreach($fields['maintenances'] as $a){
                    $i++; ?>
                    <div class="box-value clearfix<?php if($i % 2 == 0) echo ' revert';?>">
                        <div class="text">
                            <div class="ttl-sub"><?php echo $a['title']; ?></div>
                            <?php echo $a['description']; ?>
                        </div>
                        <div class="image">
                            <img src="<?php echo $a['image']; ?>" alt="<?php echo $a['title']; ?>" />
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if(isset($fields['inspections']) && !empty($fields['inspections'])){ ?>
        <div class="n-inspection custom_1">
            <div class="n-wrapper">
                <h2 class="ttl-inspection">点検</h2>
                <div class="content">
                    <ul>
                        <?php foreach($fields['inspections'] as $in){ ?>
                            <li><?php echo $in['title']; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php get_template_part('includes/6reason'); ?>
<?php endwhile; endif; ?>
<?php get_footer('hoan'); ?>