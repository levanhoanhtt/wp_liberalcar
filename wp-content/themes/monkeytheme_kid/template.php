<?php /* Template Name: template */ ?>
<?php wp_head(); ?>
<?php
if ( is_front_page() ) :
    get_header( 'home' );
elseif ( is_page( 'About' ) ) :
    get_header( 'about' );
else:
    get_header(1);
endif;
?>


<div class="post"> 
  <!--ループ開始-->
  
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <div class="kizi">
    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>
    <?php the_content(); ?>
<?php wp_link_pages(); ?>
  </div>
  <div style="padding:20px 0px;">
    <?php get_template_part('ad');?>
  </div>
  <?php endwhile; else: ?>
  <p>記事がありません</p>
  <?php endif; ?>
  <!--ループ終了--> 
  
</div>
<!-- END div.post -->

<?php get_footer(); ?>