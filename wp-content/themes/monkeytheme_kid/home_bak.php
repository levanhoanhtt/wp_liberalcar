<?php get_header(); ?>
    <div class="archive-posts cat-posts">
        <ul class="category_list">
            <?php
            $cat_all = get_terms("category", "fields=all&get=all");
            foreach ($cat_all as $value):
                ?>
                <li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <!--ループ開始-->
        <div id="dendo"></div>
        <!-- /#dendo -->
        <div class="post-wrap clearfix">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article class="entry">
                    <div class="entry-wrap clearfix">

                        <div class="sumbox"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

                                <?php if (has_post_thumbnail()): // サムネイルを持っているときの処理 ?>

                                    <?php

                                    $title = get_the_title();

                                    the_post_thumbnail(array(285, 190),

                                        array('alt' => $title, 'title' => $title)); ?>

                                <?php else: // サムネイルを持っていないときの処理 ?>

                                    <img src="<?php echo catch_that_image($post->post_content); ?>" width="285"
                                         height="190"/>

                                <?php endif; ?>

                            </a></div>

                        <!-- /.sumbox -->

                        <div class="entry-content">

                            <h3 class="entry-title"><a href="<?php the_permalink() ?>"
                                                       title="<?php the_title_attribute(); ?>">

                                    <?php the_title(); ?>

                                </a></h3>

                        </div>

                        <!-- .entry-content -->

                    </div>

                </article>

                <!--/entry-->


            <?php endwhile;
            else: ?>

                <p>記事がありません</p>

            <?php endif;
            wp_reset_query(); ?>
        </div><!--post-wrap-->
        <!--ページナビ-->
        <?php if (function_exists("pagination")) {
            pagination($wp_query->max_num_pages);
        } ?>
        <!--ループ終了-->
    </div>
    <!-- END div.post -->
<?php get_footer(); ?>