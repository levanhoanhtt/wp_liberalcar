// jQuery(function(){
// 	hTop = $('.smanone').offset().top;
// });
$(window).scroll(function () {
	if($(window).scrollTop() > 0) {
		$('.smanone').addClass('navbarfixed');
	}else{
		$('.smanone').removeClass('navbarfixed');
	}
});
$(function(){
    // ★　任意のズレ高さピクセル数を入力　↓
    var headerHight = 100;
    // #で始まるアンカーをクリックした場合に処理
    jQuery('#sub a[href^=#]').click(function() {
        // スクロールの速度
        var speed = 500; // ミリ秒
        // アンカーの値取得
        var href= jQuery(this).attr("href");
        // 移動先を取得
        var target = jQuery(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        if($('.smanone').hasClass('navbarfixed')) {
              var position = target.offset().top-headerHight; 
        }else {
             var position = target.offset().top-headerHight*2;
        }
       
        // スムーズスクロール
        jQuery('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
});


jQuery(function() {
	var pageTop = jQuery('.backtotop');
	pageTop.hide();
	//スクロールが400に達したら表示
	jQuery(window).scroll(function () {
	    if(jQuery(this).scrollTop() > 300) {
	        pageTop.fadeIn();
	    } else {
	        pageTop.fadeOut();
	    }
	});
	//スクロールしてトップ
    pageTop.click(function () {
    jQuery('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
    });
});